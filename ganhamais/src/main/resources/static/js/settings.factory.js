;
(function(angular) {
    'use strict';
    
    angular
    	.module('GanhaMaisApp')
    	.factory('settings', settings);
    
    settings.$inject = ['$rootScope'];
    
    function settings($rootScope) {
    	// supported languages
        var settings = {            
            assetsPath: 'assets',
            globalPath: 'assets/global',
            layoutPath: 'assets/layouts/layout'
        };

        $rootScope.settings = settings;

        return settings;
    }
    
})(angular);