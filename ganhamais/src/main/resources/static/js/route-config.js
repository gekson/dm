;
(function(angular) {
	'use strict';

	angular
		.module('GanhaMaisApp')
		.config(routes);

	routes.$inject = ['$stateProvider', '$urlRouterProvider'];

	function routes($stateProvider, $urlRouterProvider) {
		
		$stateProvider
		
		.state('ganhaMais', {
            url: '/',
            templateUrl: './dashboard.html',	
			controller: 'AppController'
		})
		
		.state('dash', {
            url: '/dash',
            templateUrl: './dashboard.html',	
			controller: 'AppController'
		})
		
		.state('user', {
            url: '/user',
			controller: 'AppController'
		})				
		
		.state('teste', {
            url: '/teste',
			controller: 'AlunoController',
			templateUrl: './views/aluno/lista.html',
		})
				
//		$urlRouterProvider.otherwise("ganhaMais");
		$urlRouterProvider.otherwise(function ($injector) {
			var $state = $injector.get('$state');
			$state.go('ganhaMais');
		});
	}
	
//	function config($urlRouterProvider, $stateProvider) {
//
//		$stateProvider
//
//		.state('sla', {
//			url: '/',
//			templateUrl: __sla('/partials/incidentes.html'),
//			controller: 'SLAController',
//			controllerAs: 'slaCtrl',
//			resolve: {
//				sistemas: ['$q', '$http', function ($q, $http) {
//					return $q(function (resolve, reject) {
//						$http.get(__sla('/projects'))
//							.then(function (res) {
//								resolve(res.data);
//							}, function (err) {
//								console.error(err);
//							alert('erro ao buscar os sistemas: ' + err.data || "Request failed");
//								resolve([]);
//							});
//					});
//				}]
//			}
//		})
//
//		.state('feriados', {
//			url: '/feriado',
//			templateUrl: __sla('/partials/feriados.html'),
//			controller: 'FeriadosController',
//			controllerAs: 'feriadosCtrl'
//		});
//
//		$urlRouterProvider.otherwise(function ($injector) {
//			var $state = $injector.get('$state');
//			$state.go('sla');
//		});
//	} 
})(angular);