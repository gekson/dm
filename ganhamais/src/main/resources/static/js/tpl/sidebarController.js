;
(function (angular) {
    'use strict';
    
	angular
		.module('GanhaMaisApp')
		.controller('SidebarController', SidebarController);
	
	SidebarController.$inject = ['$scope'];
	
	function SidebarController($scope) {
		var sidebarCtrl = this;
		
		$scope.$on('$includeContentLoaded', function() {
	        //Layout.initSidebar(); // init sidebar
	    });
	}
	
})(angular);