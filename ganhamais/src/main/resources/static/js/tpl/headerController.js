;
(function (angular) {
    'use strict';
    
	angular
		.module('LicceuApp')
		.controller('HeaderController', HeaderController);
	
	HeaderController.$inject = ['$scope'];

	/* Setup Layout Part - Header */
	function HeaderController($scope) {
		var headerCtrl = this;
		
		$scope.$on('$includeContentLoaded', function() {
	        Layout.initHeader(); // init header
	    });
	}
	
})(angular);