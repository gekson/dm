;
(function (angular) {
    'use strict';
    
	angular
		.module('GanhaMaisApp')
		.controller('FooterController', FooterController);
	
	FooterController.$inject = ['$scope'];
	
	/* Setup Layout Part - Footer */
	function FooterController($scope) {
		var footerCtrl = this;
		$scope.$on('$includeContentLoaded', function() {
	        //Layout.initFooter(); // init footer
	    });
	}
	
})(angular);