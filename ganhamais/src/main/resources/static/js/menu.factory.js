;
(function(angular) {
	'use strict';
	
	angular
		.module('GanhaMaisApp')
    .factory('menuFactory', menuFactory);
	
//	menuFactory.$inject = ['$rootScope'];
    
    function menuFactory() {
    	var serviceInstance = {
    		     menu : menu
    		  };
    	
    	function menu(menuAtual, $rootScope) {
        	if(menuAtual == 'aluno'){
        		$rootScope.alunosMenu = true;
        		$rootScope.cargaMenu = false;
        		$rootScope.produtosMenu = false;
        		$rootScope.pontosMenu = false;
        		$rootScope.pontosAlunoMenu = false;
        		$rootScope.usuarioMenu = false;
        		$rootScope.categoriaMenu = false;
        		$rootScope.paginaAtual = "Alunos";
        	}
        	
        	if(menuAtual == 'cargaAluno'){
        		$rootScope.alunosMenu = false;
        		$rootScope.cargaMenu = true;
        		$rootScope.produtosMenu = false;
        		$rootScope.pontosMenu = false;
        		$rootScope.pontosAlunoMenu = false;
        		$rootScope.usuarioMenu = false;
        		$rootScope.categoriaMenu = false;
        		$rootScope.paginaAtual = "Carga de Alunos";
        	}
        	 
        	if(menuAtual == 'produto'){
        		$rootScope.alunosMenu = false;
        		$rootScope.cargaMenu = false;
        		$rootScope.produtosMenu = true;
        		$rootScope.pontosMenu = false;   
        		$rootScope.pontosAlunoMenu = false;
        		$rootScope.usuarioMenu = false;
        		$rootScope.categoriaMenu = false;
        		$rootScope.paginaAtual = "Produtos";
        	}
        	
        	if(menuAtual == 'ponto'){
        		$rootScope.alunosMenu = false;
        		$rootScope.cargaMenu = false;
        		$rootScope.produtosMenu = false;
        		$rootScope.pontosMenu = true;
        		$rootScope.pontosAlunoMenu = false;
        		$rootScope.usuarioMenu = false;
        		$rootScope.categoriaMenu = false;
        		$rootScope.paginaAtual = "Pontos";
        	}
        	
        	if(menuAtual == 'pontoAluno'){
        		$rootScope.alunosMenu = false;
        		$rootScope.cargaMenu = false;
        		$rootScope.produtosMenu = false;
        		$rootScope.pontosMenu = false;
        		$rootScope.pontosAlunoMenu = true;
        		$rootScope.usuarioMenu = false;
        		$rootScope.categoriaMenu = false;
        		$rootScope.paginaAtual = "Pontos do Aluno";
        	}
        	
        	if(menuAtual == 'usuario'){        		
        		$rootScope.alunosMenu = false;
        		$rootScope.cargaMenu = false;
        		$rootScope.produtosMenu = false;
        		$rootScope.pontosMenu = false;
        		$rootScope.pontosAlunoMenu = false;
        		$rootScope.usuarioMenu = true;
        		$rootScope.categoriaMenu = false;
        		$rootScope.paginaAtual = "Usuários";
        	}
        	
        	if(menuAtual == 'categoria'){        		
        		$rootScope.alunosMenu = false;
        		$rootScope.cargaMenu = false;
        		$rootScope.produtosMenu = false;
        		$rootScope.pontosMenu = false;
        		$rootScope.pontosAlunoMenu = false;
        		$rootScope.usuarioMenu = false;
        		$rootScope.categoriaMenu = true;
        		$rootScope.paginaAtual = "Categorias";
        	}
        }
    	
    	return serviceInstance;
    }
     
})(angular);