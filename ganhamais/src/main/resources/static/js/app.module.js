;
(function(angular) {
    'use strict';
    
	angular
		.module('GanhaMaisApp', [		               
		    'ngAnimate', 
		    'ngSanitize', 
		    'ui.bootstrap',
            "ui.router",
            "ngMaterial",
            "ngMessages",
            "oc.lazyLoad",
            "ngFileUpload",
            "app.aluno",
            "app.login",
            "app.ponto",
            "app.produto",
            "app.usuario",
            "app.categoria"
		])
		.config(controllerProvider)
		.config(header)
		.config(ocLazyLoadProvider)
		.run(appRun);
	
	controllerProvider.$inject = ['$controllerProvider', '$logProvider'];
	
	header.$inject = ['$httpProvider'];
	
	ocLazyLoadProvider.$inject = ['$ocLazyLoadProvider'];
	
	appRun.$inject = ['$rootScope', 'settings', '$state'];

	/* Init global settings and run the app */
	function appRun($rootScope, settings, $state, $logProvider) {
	    $rootScope.$state = $state; // state to be accessed from view
	    $rootScope.$settings = settings; // state to be accessed from view
	}
	
	function header($httpProvider) {
        $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
	}
	
	//AngularJS v1.3.x workaround for old style controller declarition in HTML
	function controllerProvider($controllerProvider, $logProvider) {
	  // this option might be handy for migrating old apps, but please don't use it
	  // in new ones!
	  $controllerProvider.allowGlobals();
	  $logProvider.debugEnabled(false);
	}
	
	function ocLazyLoadProvider($ocLazyLoadProvider) {
		$ocLazyLoadProvider.config({
			// global configs go here
		});
	}
	
})(angular);