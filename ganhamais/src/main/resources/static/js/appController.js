;
(function(angular) {
    'use strict';
    
	angular
		.module('GanhaMaisApp')
		.controller('AppController', AppController);
	
	AppController.$inject = ['$http', '$rootScope', '$location', '$state'];

	/* Setup App Main Controller */
	function AppController($http, $rootScope, $location, $state) {
        
		var vm = this;				
		
		vm.usuario;
		vm.senha;
		vm.login = login;
		vm.logout = logout;
		vm.alteraSenhaUsuarioLogado = alteraSenhaUsuarioLogado;
		
		$rootScope.authorites = [];        	
		$rootScope.alunosMenu = false;
		$rootScope.produtosMenu = false;
		$rootScope.pontosMenu = false;
		$rootScope.pontosAlunoMenu = false;
		$rootScope.cargaMenu = false;
		$rootScope.usuarioMenu = false;
		$rootScope.categoriaMenu = false;
		$rootScope.paginaAtual = "";
		$rootScope.editProduto == false;
        
		function authenticate(callback) {			
			$rootScope.authenticated = false;
        };
        
        function logout() {
        	$rootScope.escondeMenu = false;
        	$rootScope.authenticated = false;  
        }
        
        function login(){        	
        	$http({
        		method : 'POST',
                url: '/usuario/findUsuarioByLoginAndSenha',
                data: {"matricula" : vm.usuario, "senha":vm.senha}
                }).success(function(data) {                   	
                	if(data.id) { 
                		if(data.isAluno){
                			$rootScope.authenticated = false;
                            alert("Usuário não autorizado.");
                            return;
                		}
                        $rootScope.authenticated = true;       
//                        $location.path('/dashboard.html'); 
                        $rootScope.escondeMenu = true;
                        $rootScope.usuarioLogado = data;
//                        $state.go('dash');
//                        $rootScope.authorites = data.authorites;
                    } else {
                        $rootScope.authenticated = false;
                        alert("Usuário ou senha inválido.");
                    }                	
                })
                .error(function(e) {
                	alert("Usuúrio ou senha inválido.");
                	$rootScope.authenticated = false;
                });
        }        
        
        authenticate(function() {
        	if($rootScope.authenticated) {
        		alert("IF authenticated ");
        		$location.path('/dashboard.html');        		 
        	} else {
        		alert("Else");
        		_redirectLogin();
        	}
        });
        
        function _redirectLogin() {
        	$location.path('/login.html'); 
        }
        
        function alteraSenhaUsuarioLogado() {
        	$rootScope.usuarioTrocaSenha = $rootScope.usuarioLogado;
        	$state.go('alteraSenha');
        }
		
	}
	
})(angular);