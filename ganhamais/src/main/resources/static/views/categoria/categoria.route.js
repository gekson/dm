;
(function(angular) {
	'use strict';
	
	angular
		.module('app.categoria')
        .config(route);
    
    route.$inject = ['$stateProvider'];
    
    function route($stateProvider) {
        $stateProvider
        
	        .state('listaCategoria', {
	            url: '/listaCategoria',
	            templateUrl: './views/categoria/lista.html',
	            controller: 'CategoriaController',
                controllerAs: 'categoriaCtrl'
	        })
        
        .state('cadastroCategoria', {
            url: '/cadastroCategoria',
            templateUrl: './views/categoria/cadastro.html',
            controller: 'CategoriaController',            
            controllerAs: 'categoriaCtrl'
        })
        .state('editCategoria', {
            url: '/editCategoria',
            templateUrl: './views/categoria/edit.html',
            controller: 'CategoriaEditController',
            params:      {'categoria': null},
            controllerAs: 'categoriaEditCtrl'
        });
    }
	
})(angular);