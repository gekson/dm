;
(function(angular) {
	'use strict';
	
	angular
		.module('app.categoria')
		.controller('CategoriaEditController', CategoriaEditController)
		.directive('chooseFile', function() {
    return {
      link: function (scope, elem, attrs) {
        var button = elem.find('button');
        var input = angular.element(elem[0].querySelector('input#fileInput'));
        button.bind('click', function() {
          input[0].click();
        });
        input.bind('change', function(e) {
          scope.$apply(function() {
            var files = e.target.files;
            if (files[0]) {
              scope.fileName = files[0].name;
              scope.fileImagem = files[0];
            } else {
              scope.fileName = null;
            }
          });
        });
      }
    };
  });
    
	CategoriaEditController.$inject = ['$q', '$location', '$http',  '$timeout', '$window', '$scope', '$rootScope', 'menuFactory', '$mdDialog', '$uibModal', '$state', 'Upload', '$stateParams'];
	
	function CategoriaEditController($q, $location, $http, $timeout, $window, $scope, $rootScope, menuFactory, $mdDialog, $uibModal, $state, Upload, $stateParams) {
		var vm = this;		                
		vm.categorias={};                
        vm.saveCategoria = saveCategoria;              
        vm.categoria = $stateParams.categoria;
        
        vm.medidas = ('Evento Mensal Participação Unidade')
    	.split(' ').map(function (medida) { return { nome: medida }; });       
            
    function saveCategoria(){        	
		$http({
    		method : 'POST',
            url: '/categoria/salvarCategoria',
            data: {"nome": vm.categoria.nome, "pontos": vm.categoria.pontos, "coletivo": vm.categoria.coletivo, 
            	"ativo":vm.categoria.ativo, "unidadeMedida":vm.categoria.unidadeMedida,
            	"porUnidade":vm.categoria.porUnidade, "pontoVariavel":vm.categoria.pontoVariavel}
            }).success(function(data) {                	
            	$mdDialog.show(
            		      $mdDialog.alert()
            		        .parent(angular.element(document.querySelector('#popupContainer')))
            		        .clickOutsideToClose(false)
            		        .title('Sucesso')
            		        .textContent('Categoria cadastrado com sucesso.')
            		        .ariaLabel('Sucesso')
            		        .ok('Ok!')
            		    ).then(function(answer) {
            		    	$state.go('listaCategoria');
            	    	    });                    	
            })
            .error(function(e) {
            	$mdDialog.show(
          		      $mdDialog.alert()
          		        .parent(angular.element(document.querySelector('#popupContainer')))
          		        .clickOutsideToClose(false)
          		        .title('Erro')
          		        .textContent('O Categoria não foi cadastrado. Ocorreu um erro.')
          		        .ariaLabel('Erro')
          		        .ok('Ok!')
          		    )
            });					
    }
        
        function upload() {
        	Upload.upload({
				url: '/categoria/editCategoriaComFoto',
				fields: {'nome': vm.categoria.nome,
					'destaque': vm.categoria.destaque},
				data: {arquivo: $scope.fileImagem,
					'nome': vm.categoria.nome,
					'valor': vm.categoria.valor,					
					'destaque': vm.categoria.destaque,
					'descricao': vm.categoria.descricao, 'id': vm.categoria.id}
			}).success(function(data) {            						
            	$mdDialog.show(
            		      $mdDialog.alert()
            		        .parent(angular.element(document.querySelector('#popupContainer')))
            		        .clickOutsideToClose(false)
            		        .title('Sucesso')
            		        .textContent('Categoria alterado com sucesso.')
            		        .ariaLabel('Sucesso')
            		        .ok('Ok!')
            		    ).then(function(answer) {
            		    	$state.go('listaCategoria');
            	    	    });                    	
            })
            .error(function(e) {
            	$mdDialog.show(
          		      $mdDialog.alert()
          		        .parent(angular.element(document.querySelector('#popupContainer')))
          		        .clickOutsideToClose(false)
          		        .title('Erro')
          		        .textContent('O Categoria não foi alterado. Ocorreu um erro.'+e.message)
          		        .ariaLabel('Erro')
          		        .ok('Ok!')
          		    )
            });
        }
         
        function getCategorias(){
        	$http
            .get('/categoria')
                .success(function(data) {
                    vm.categorias = data;
                });  
        }
        
        
        function iniciar() {        	
        	getCategorias();
        	menuFactory.menu('categoria',$rootScope);
        }
        
    	iniciar();
    	
	}		
	
})(angular);