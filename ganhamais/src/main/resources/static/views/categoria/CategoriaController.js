;
(function(angular) {
	'use strict';
	
	angular
		.module('app.categoria')
		.controller('CategoriaController', CategoriaController);
    
	CategoriaController.$inject = ['$q', '$location', '$http',  '$timeout', '$window', '$scope', '$rootScope', 'menuFactory', '$mdDialog', '$uibModal', '$state', 'Upload', '$stateParams'];
	
	function CategoriaController($q, $location, $http, $timeout, $window, $scope, $rootScope, menuFactory, $mdDialog, $uibModal, $state, Upload, $stateParams) {
		var vm = this;		                
        vm.categorias={};                
        vm.saveCategoria = saveCategoria;
        vm.editCategoria = editCategoria;
        vm.excluiCategoria = excluiCategoria;        

    	vm.categoria = {
                nome: "",
                pontos: "",
                coletivo: false,
                ativo: true,
                unidadeMedida: "",
                porUnidade: false,
                pontoVariavel: false,
                mesUnidade: ""
            }	
        
        vm.medidas = ('Evento Mensal Participação Unidade')
        	.split(' ').map(function (medida) { return { nome: medida }; });       
                
        function saveCategoria(){        	
			$http({
        		method : 'POST',
                url: '/categoria/salvarCategoria',
                data: {"nome": vm.categoria.nome, "pontos": vm.categoria.pontos, "coletivo": vm.categoria.coletivo, 
                	"ativo":vm.categoria.ativo, "unidadeMedida":vm.categoria.unidadeMedida,
                	"porUnidade":vm.categoria.porUnidade, "pontoVariavel":vm.categoria.pontoVariavel}
                }).success(function(data) {                	
                	$mdDialog.show(
                		      $mdDialog.alert()
                		        .parent(angular.element(document.querySelector('#popupContainer')))
                		        .clickOutsideToClose(false)
                		        .title('Sucesso')
                		        .textContent('Categoria cadastrado com sucesso.')
                		        .ariaLabel('Sucesso')
                		        .ok('Ok!')
                		    ).then(function(answer) {
                		    	$state.go('listaCategoria');
                	    	    });                    	
                })
                .error(function(e) {
                	$mdDialog.show(
              		      $mdDialog.alert()
              		        .parent(angular.element(document.querySelector('#popupContainer')))
              		        .clickOutsideToClose(false)
              		        .title('Erro')
              		        .textContent('O Categoria não foi cadastrado. Ocorreu um erro.')
              		        .ariaLabel('Erro')
              		        .ok('Ok!')
              		    )
                });					
        }
                
        function addCategoria(ev) {
        	$uibModal.open({
                animation: true,
                templateUrl: './views/categoria/cadastro.html',
                controller: ['$uibModalInstance',
                function ($uibModalInstance) {
                    var modal = this;
                    modal.title = 'Aviso';                    
                    modal.confirmLabel = 'Sim';
                    modal.cancelLabel = 'Não';

                    modal.confirm = function confirm() {                        
                    	$uibModalInstance.close();
                    };

                    modal.close = function close() {
                        $modalInstance.close();
                    };
                }],
                controllerAs: 'confirmCrt',
                keyboard: false,
                backdrop: 'static',
                size: 'sm'                
            });
        }
        
        function editCategoria(ev) {
        	vm.categoria = ev;        	
        	$state.go('editCategoria', {'categoria':ev});
//        	$http({
//        		method : 'POST',
//                url: '/categoria/getCategoria',
//                data: {'id':ev}
//                }).success(function(data) {
//                    //swal({text: "Buscando informações...", type: "success", timer: 2000, showConfirmButton:false, allowOutsideClick:false});
//                    //vm.hubLink={};                    	
//                })
//                .error(function(e) {
//                });
        }
        
        function excluiCategoria(ev) {
        	if($window.confirm("Deseja realmente excluir esse categoria?")){
        		$http
                .delete('/categoria/excluiCategoria/'+ev.id)
                    .success(function(data) {
                    	$mdDialog.show(
                  		      $mdDialog.alert()
                  		        .parent(angular.element(document.querySelector('#popupContainer')))
                  		        .clickOutsideToClose(false)
                  		        .title('Sucesso')
                  		        .textContent('Categoria excluido com sucesso.')
                  		        .ariaLabel('Sucesso')
                  		        .ok('Ok!')    
                  		).then(function (anwser) {
                    		iniciar();
                    	});
                    })
                    .error(function(e) {
                    	$mdDialog.show(
                  		      $mdDialog.alert()
                  		        .parent(angular.element(document.querySelector('#popupContainer')))
                  		        .clickOutsideToClose(false)
                  		        .title('Erro')
                  		        .textContent('Erro ao excluir o registro.' +e.message)
                  		        .ariaLabel('Erro')
                  		        .ok('Ok!')
                    	)
                      });  
        	}        	
        }
        
        function getCategorias(){
        	$http
            .get('/categoria')
                .success(function(data) {
                    vm.categorias = data;
                });  
        }
        
        
        function iniciar() {        	
        	getCategorias();
        	menuFactory.menu('categoria',$rootScope);
        }
        
    	iniciar();
    	
	}
	
	function CategoriaDialogCtrl ($scope, $rootScope, $mdDialog) {
		  var $ctrl = this;
		  $ctrl.toggleAll = toggleAll;
		  $ctrl.exists = exists;
		  $ctrl.toggle = toggle;
		  
//		  $ctrl.items = alunos;
//		  $ctrl.selected = {
//		    item: $ctrl.items[0]
//		  };
		  $ctrl.selected = ['Aproveitamento Coletivo'];
		  
		  $ctrl.isIndeterminate = false;
		  $ctrl.isChecked = false;
		    
		  $ctrl.ok = function () {
		    $uibModalInstance.close($ctrl.selected.item);
		  };

		  $ctrl.cancel = function () {
		    $uibModalInstance.dismiss('cancel');
		  };
		  
		   function toggle(item, list) {
		        var idx = list.indexOf(item);
		        if (idx > -1) {
		          list.splice(idx, 1);
		        }
		        else {
		          list.push(item);
		        }
		      }
		    
		   function exists(item, list) {
		        return list.indexOf(item) > -1;
		      };

		      $ctrl.isIndeterminate = function() {
		        return ($ctrl.selected.length !== 0 &&
		        		$ctrl.selected.length !== $ctrl.items.length);
		      };

		      $ctrl.isChecked = function() {
		        return $ctrl.selected.length === $ctrl.items.length;
		      };

		      function toggleAll()  {
		        if ($ctrl.selected.length === $ctrl.items.length) {
		        	$ctrl.selected = [];
		        } else if ($ctrl.selected.length === 0 || $ctrl.selected.length > 0) {
		        	$ctrl.selected = $ctrl.items.slice(0);
		        }
		      };
		}
	
})(angular);