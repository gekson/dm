;
(function(angular) {
	'use strict';
	
	angular
		.module('app.aluno')
		.controller('CargaAlunoController', CargaAlunoController)
		.directive('chooseFile', function() {
    return {
      link: function (scope, elem, attrs) {
        var button = elem.find('button');
        var input = angular.element(elem[0].querySelector('input#fileInput'));
        button.bind('click', function() {
          input[0].click();
        });
        input.bind('change', function(e) {
          scope.$apply(function() {
            var files = e.target.files;
            if (files[0]) {
              scope.fileNameCarga = files[0].name;
              scope.fileCarga = files[0];
            } else {
              scope.fileNameCarga = null;
            }
          });
        });
      }
    };
  });
    
	CargaAlunoController.$inject = ['$q', '$location', '$http',  '$timeout', '$window', '$scope', '$rootScope', 'menuFactory', 'Upload', '$mdDialog'];
	
	function CargaAlunoController($q, $location, $http, $timeout, $window, $scope, $rootScope, menuFactory, Upload, $mdDialog) {
		var vm = this;		                        
        vm.cargaArquivo = cargaArquivo;                
        
        function cargaArquivo(){        	
        	if($scope.fileCarga == null || $scope.fileCarga == undefined){
        		return;
        	}
        	showWait();
        	Upload.upload({
				url: '/aluno/cargaArquivo',
				data: {arquivo: $scope.fileCarga}
			}).success(function(data) {
				setTimeout(function(){                            
            		hideWait();
                  },2000);
            	$mdDialog.show(
            		      $mdDialog.alert()
            		        .parent(angular.element(document.querySelector('#popupContainer')))
            		        .clickOutsideToClose(false)
            		        .title('Sucesso')
            		        .textContent('Carga realizada com sucesso.')
            		        .ariaLabel('Sucesso')
            		        .ok('Ok!')
            		    ).then(function(answer) {
            		    	$state.go('listaAluno');
            	    	    });                    	
            })
            .error(function(e) {
            	$mdDialog.show(
          		      $mdDialog.alert()
          		        .parent(angular.element(document.querySelector('#popupContainer')))
          		        .clickOutsideToClose(false)
          		        .title('Erro')
          		        .textContent('Carga não realizada. Ocorreu um erro.'+e)
          		        .ariaLabel('Erro')
          		        .ok('Ok!')
          		    )
            });
        }
        
        function iniciar() {        	  
        	menuFactory.menu('cargaAluno',$rootScope);
        }
        
        function hideWait(){
            setTimeout(function(){
            	$mdDialog.cancel(); 
                  },5);
        }
        
       function showWait(){
                $mdDialog.show({                  
                  template: '<md-dialog id="plz_wait" style="height:100px;background-color:transparent;box-shadow:none">' +
                              '<div layout="row" layout-sm="column" layout-align="center center" aria-label="wait">' +
                                  '<md-progress-circular md-mode="indeterminate" ></md-progress-circular>' +
                              '</div>' +
                           '</md-dialog>',
                  parent: angular.element(document.querySelector('#alunos')),
                  clickOutsideToClose:false,
                  fullscreen: false
                })
                .then(function(answer) {
                  
                });
        }
       
    	iniciar();
    	
	}
	
})(angular);