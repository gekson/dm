;
(function(angular) {
	'use strict';
	
	angular
		.module('app.aluno')
		.controller('AlunoController', AlunoController);
    
	AlunoController.$inject = ['$q', '$location', '$http',  '$timeout', '$window', '$scope', '$rootScope', 'menuFactory', '$mdDialog', '$state'];
	
	function AlunoController($q, $location, $http, $timeout, $window, $scope, $rootScope, menuFactory, $mdDialog, $state) {
		var vm = this;		                
        vm.alunos={};
        vm.feeds={};
        vm.buscaAluno = buscaAluno;
        vm.isLoading = true;
        vm.showWait = showWait;
        vm.hideWait = hideWait;
        vm.showAlteraSenha = showAlteraSenha;
        vm.alteraSenha = alteraSenha;
        vm.showExtrato = showExtrato;
        
        vm.aluno = {
                nome: "",
                matricula: "",
                id: 0
            };
        vm.usuario = {
        		login: "",
        		senha: ""
        };
        if($rootScope.usuarioTrocaSenha != null){
        	vm.usuario = $rootScope.usuarioTrocaSenha;
        }
        
        function getAlunos(){
        	showWait();
        	$http({
            		method : 'GET',
                    url: '/aluno',
                    data: vm.hubLink
                    }).success(function(data) {                    	
                    	vm.alunos = data;
                    	vm.isLoading = false;
                         
                    	setTimeout(function(){                            
                    		hideWait();
                          },2000);
                    })
                    .error(function(e) {
                    	setTimeout(function(){                            
                    		hideWait();
                          },2000);                    	
                    });
        } 
        
        function hideWait(){
            setTimeout(function(){
            	$mdDialog.cancel(); 
                  },5);
        }
        
       function showWait(){
                $mdDialog.show({                  
                  template: '<md-dialog id="plz_wait" style="height:100px;background-color:transparent;box-shadow:none">' +
                              '<div layout="row" layout-sm="column" layout-align="center center" aria-label="wait">' +
                                  '<md-progress-circular md-mode="indeterminate" ></md-progress-circular>' +
                              '</div>' +
                           '</md-dialog>',
                  parent: angular.element(document.querySelector('#alunos')),
                  clickOutsideToClose:false,
                  fullscreen: false
                })
                .then(function(answer) {
                  
                });
        }
        
        function buscaAluno() {
        	if(vm.aluno.matricula=="") {
        		getAlunos();
        		return;
        	}
	    	$http({
        		method : 'POST',
                url: '/aluno/findAlunoByMatricula',
                data: vm.aluno
                }).success(function(data) {                	
                	vm.alunos = data;                	                	
                })
                .error(function(e) {
                	$mdDialog.show(
              		      $mdDialog.alert()
              		        .parent(angular.element(document.querySelector('#alunos')))
              		        .clickOutsideToClose(false)
              		        .title('Erro')
              		        .textContent('Aluno não cadastrado')
              		        .ariaLabel('Erro')
              		        .ok('Ok!')
              		    )
                });
	    }
        
        function iniciar() {        	
        	getAlunos();    
        	menuFactory.menu('aluno',$rootScope);
        }
        
        function showAlteraSenha(a) {
        	$http({
        		method : 'POST',
                url: '/usuario/findUsuarioByLogin',
                data: {"matricula": a.matricula}
                }).success(function(data) {  
                	$rootScope.usuarioTrocaSenha = data;
                	$state.go('alteraSenha');
                })
                .error(function(e) {
                	$mdDialog.show(
              		      $mdDialog.alert()
              		        .parent(angular.element(document.querySelector('#alunos')))
              		        .clickOutsideToClose(false)
              		        .title('Erro')
              		        .textContent('Erro ao recuperar usuário do Aluno.')
              		        .ariaLabel('Erro')
              		        .ok('Ok!')
              		    )
                });
        }
        
        function alteraSenha(){
        	$http({
        		method : 'POST',
                url: '/usuario/alteraSenha',
                data: {"id":vm.usuario.id, "senha":vm.usuario.senha}
                }).success(function(data) {  
                	$mdDialog.show(
                		      $mdDialog.alert()
                		        .parent(angular.element(document.querySelector('#alunos')))
                		        .clickOutsideToClose(false)
                		        .title('Sucesso')
                		        .textContent('Senha alterada com sucesso.')
                		        .ariaLabel('Sucesso')
                		        .ok('Ok!')
                		    )
                	$state.go('listaAluno');
                })
                .error(function(e) {
                	$mdDialog.show(
              		      $mdDialog.alert()
              		        .parent(angular.element(document.querySelector('#alunos')))
              		        .clickOutsideToClose(false)
              		        .title('Erro')
              		        .textContent('Erro ao alterar a senha.')
              		        .ariaLabel('Erro')
              		        .ok('Ok!')
              		    )
                });
        }
        
        function showExtrato(ev) {	    	
	    	$mdDialog.show({
	    		controller: DialogCtrl,
	    		controllerAs: 'dialogCtrl',
	    	      templateUrl: './views/aluno/extrato.html',
	    	      parent: angular.element(document.body),
	    	      targetEvent: ev,
	    	      clickOutsideToClose:true,
	    	      fullscreen: true, // Only for -xs, -sm breakpoints.
	    	      locals : {
		              aluno:  ev
		            }
	    	    })
	    	    .then(function(answer) {
	    	      
	    	    }, function() {
	    	      
	    	    });
	    }
        
        function DialogCtrl ($scope, $rootScope, $mdDialog, $http, aluno) {
  		  var $ctrl = this;  		  
  		  $ctrl.aluno = aluno;
  		  
  		}
        
    	iniciar();
    	
	}
	
})(angular);