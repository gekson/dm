;
(function(angular) {
	'use strict';
	
	angular
		.module('app.aluno')
        .config(route);
    
    route.$inject = ['$stateProvider'];
    
    function route($stateProvider) {
        $stateProvider
        
	        .state('cargaArquivo', {
	            url: '/cargaArquivo',
	            templateUrl: './views/aluno/cargaAlunos.html',
	            controller: 'CargaAlunoController',
	            controllerAs: 'cargaAlunoCtrl'
	        })
	        .state('listaAluno', {
	            url: '/listaAluno',
	            templateUrl: './views/aluno/lista.html',
	            data: {pageTitle: 'LISTA DE ALUNOS'},
	            controller: 'AlunoController',
                controllerAs: 'alunoCtrl'
	        })
	        .state('alteraSenha', {
	            url: '/alteraSenha',
	            templateUrl: './views/aluno/altera_senha.html',
	            data: {pageTitle: 'Altera Senha'},
	            controller: 'AlunoController',
                controllerAs: 'alunoCtrl'
	        });
    }
	
})(angular);