;
(function(angular) {
	'use strict';
	
	angular
		.module('app.produto')
		.controller('ProdutoController', ProdutoController)
		.directive('chooseFile', function() {
    return {
      link: function (scope, elem, attrs) {
        var button = elem.find('button');
        var input = angular.element(elem[0].querySelector('input#fileInput'));
        button.bind('click', function() {
          input[0].click();
        });
        input.bind('change', function(e) {
          scope.$apply(function() {
            var files = e.target.files;
            if (files[0]) {
              scope.fileName = files[0].name;
              scope.fileImagem = files[0];
            } else {
              scope.fileName = null;
            }
          });
        });
      }
    };
  });
    
	ProdutoController.$inject = ['$q', '$location', '$http',  '$timeout', '$window', '$scope', '$rootScope', 'menuFactory', '$mdDialog', '$uibModal', '$state', 'Upload', '$stateParams'];
	
	function ProdutoController($q, $location, $http, $timeout, $window, $scope, $rootScope, menuFactory, $mdDialog, $uibModal, $state, Upload, $stateParams) {
		var vm = this;		                
        vm.alunos={};        
        vm.addProduto = addProduto;
        vm.saveProduto = saveProduto;
        vm.editProduto = editProduto;
        vm.excluiProduto = excluiProduto;
        vm.usePonto = usePonto;

    	vm.produto = {
                nome: "",
                valor: "",
                destaque: false,
                descricao: ""
            }	
        
        vm.teste = "self";
        vm.produtos = [
	                       { nome: 'Bola', pontos:10, wanted: true },
	                       { nome: 'TV', pontos:500, wanted: false },
	                       { nome: 'Bicicleta', pontos:120, wanted: true },
	                       { nome: vm.teste.toUpperCase(), pontos:120, wanted: true }
	                     ];
        vm.incluiProdduto = {};
        vm.foto;
        
        
        function saveProduto(){
        	vm.incluiProdduto.produto = vm.produto;
//        	vm.incluiProdduto.imagem = $scope.fileImagem;
//        	vm.incluiProdduto.arquivo = vm.foto;
        	if($scope.fileImagem == null || $scope.fileImagem == undefined){
        		return;
        	}
        	Upload.upload({
				url: '/produto/salvarProduto',
				fields: {'nome': vm.produto.nome,
					'destaque': vm.produto.destaque},
				data: {arquivo: $scope.fileImagem,
					'nome': vm.produto.nome,
					'valor': vm.produto.valor,					
					'destaque': vm.produto.destaque,
					'descricao': vm.produto.descricao}
			}).success(function(data) {
            	//vm.alunos = data;
                //swal({text: "Buscando informações...", type: "success", timer: 2000, showConfirmButton:false, allowOutsideClick:false});
                //vm.hubLink={};
//				$http({
//            		method : 'POST',
//                    url: '/produto/salvarProduto',
//                    //data: {[vm.produto, $scope.fileName]}
//                    data: vm.incluiProdduto
//                    }).success(function(data) {
//                    	//vm.alunos = data;
//                        //swal({text: "Buscando informações...", type: "success", timer: 2000, showConfirmButton:false, allowOutsideClick:false});
//                        //vm.hubLink={};
//                    	$mdDialog.show(
//                    		      $mdDialog.alert()
//                    		        .parent(angular.element(document.querySelector('#popupContainer')))
//                    		        .clickOutsideToClose(false)
//                    		        .title('Sucesso')
//                    		        .textContent('Produto cadastrado com sucesso.')
//                    		        .ariaLabel('Sucesso')
//                    		        .ok('Ok!')
//                    		    ).then(function(answer) {
//                    		    	$state.go('listaProduto');
//                    	    	    });                    	
//                    })
//                    .error(function(e) {
//                    	$mdDialog.show(
//                  		      $mdDialog.alert()
//                  		        .parent(angular.element(document.querySelector('#popupContainer')))
//                  		        .clickOutsideToClose(false)
//                  		        .title('Erro')
//                  		        .textContent('O Produto não foi cadastrado. Ocorreu um erro.')
//                  		        .ariaLabel('Erro')
//                  		        .ok('Ok!')
//                  		    )
//                    });
					
            	$mdDialog.show(
            		      $mdDialog.alert()
            		        .parent(angular.element(document.querySelector('#popupContainer')))
            		        .clickOutsideToClose(false)
            		        .title('Sucesso')
            		        .textContent('Produto cadastrado com sucesso.')
            		        .ariaLabel('Sucesso')
            		        .ok('Ok!')
            		    ).then(function(answer) {
            		    	$state.go('listaProduto');
            	    	    });                    	
            })
            .error(function(e) {
            	$mdDialog.show(
          		      $mdDialog.alert()
          		        .parent(angular.element(document.querySelector('#popupContainer')))
          		        .clickOutsideToClose(false)
          		        .title('Erro')
          		        .textContent('O Produto não foi cadastrado. Ocorreu um erro.'+e.message)
          		        .ariaLabel('Erro')
          		        .ok('Ok!')
          		    )
            });
//        	$http({
//            		method : 'POST',
//                    url: '/produto/salvarProduto',
//                    //data: {[vm.produto, $scope.fileName]}
//                    data: vm.incluiProdduto
//                    }).success(function(data) {
//                    	//vm.alunos = data;
//                        //swal({text: "Buscando informações...", type: "success", timer: 2000, showConfirmButton:false, allowOutsideClick:false});
//                        //vm.hubLink={};
//                    	$mdDialog.show(
//                    		      $mdDialog.alert()
//                    		        .parent(angular.element(document.querySelector('#popupContainer')))
//                    		        .clickOutsideToClose(false)
//                    		        .title('Sucesso')
//                    		        .textContent('Produto cadastrado com sucesso.')
//                    		        .ariaLabel('Sucesso')
//                    		        .ok('Ok!')
//                    		    ).then(function(answer) {
//                    		    	$state.go('listaProduto');
//                    	    	    });                    	
//                    })
//                    .error(function(e) {
//                    	$mdDialog.show(
//                  		      $mdDialog.alert()
//                  		        .parent(angular.element(document.querySelector('#popupContainer')))
//                  		        .clickOutsideToClose(false)
//                  		        .title('Erro')
//                  		        .textContent('O Produto não foi cadastrado. Ocorreu um erro.')
//                  		        .ariaLabel('Erro')
//                  		        .ok('Ok!')
//                  		    )
//                    });
        }
        
//        function addProduto(ev) {
//	    	$mdDialog.show({
//	    		controller: ProdutoDialogCtrl,
//	    		controllerAs: 'produtoDialogCtrl',
//	    	      templateUrl: './views/produto/cadastro.html',
//	    	      parent: angular.element(document.body),
//	    	      targetEvent: ev,
//	    	      clickOutsideToClose:true,
//	    	      fullscreen: true	    	      
//	    	    })
//	    	    .then(function(answer) {
//	    	      $scope.status = 'You said the information was "' + answer + '".';
//	    	    }, function() {
//	    	      $scope.status = 'You cancelled the dialog.';
//	    	    });
//	    }
        
        function addProduto(ev) {
        	$uibModal.open({
                animation: true,
                templateUrl: './views/produto/cadastro.html',
                controller: ['$uibModalInstance',
                function ($uibModalInstance) {
                    var modal = this;
                    modal.title = 'Aviso';                    
                    modal.confirmLabel = 'Sim';
                    modal.cancelLabel = 'Não';

                    modal.confirm = function confirm() {                        
                    	$uibModalInstance.close();
                    };

                    modal.close = function close() {
                        $modalInstance.close();
                    };
                }],
                controllerAs: 'confirmCrt',
                keyboard: false,
                backdrop: 'static',
                size: 'sm'                
            });
        }
        
        function editProduto(ev) {
        	vm.produto = ev;        	
        	$state.go('editProduto', {'produto':ev});
//        	$http({
//        		method : 'POST',
//                url: '/produto/getProduto',
//                data: {'id':ev}
//                }).success(function(data) {
//                    //swal({text: "Buscando informações...", type: "success", timer: 2000, showConfirmButton:false, allowOutsideClick:false});
//                    //vm.hubLink={};                    	
//                })
//                .error(function(e) {
//                });
        }
        
        function excluiProduto(ev) {
        	if($window.confirm("Deseja realmente excluir esse produto?")){
        		$http
                .delete('/produto/excluiProduto/'+ev.id)
                    .success(function(data) {
                    	$mdDialog.show(
                  		      $mdDialog.alert()
                  		        .parent(angular.element(document.querySelector('#popupContainer')))
                  		        .clickOutsideToClose(false)
                  		        .title('Sucesso')
                  		        .textContent('Produto excluido com sucesso.')
                  		        .ariaLabel('Sucesso')
                  		        .ok('Ok!')    
                  		).then(function (anwser) {
                    		iniciar();
                    	});
                    })
                    .error(function(e) {
                    	$mdDialog.show(
                  		      $mdDialog.alert()
                  		        .parent(angular.element(document.querySelector('#popupContainer')))
                  		        .clickOutsideToClose(false)
                  		        .title('Erro')
                  		        .textContent('Erro ao excluir o registro.' +e.message)
                  		        .ariaLabel('Erro')
                  		        .ok('Ok!')
                    	)
                      });  
        	}        	
        }
        
        function getProdutos(){
        	$http
            .get('/produto')
                .success(function(data) {
                    vm.produtos = data;
                });  
        }
        
        function usePonto(ev) {	
        	var idsProduto = [];
        	idsProduto.push(1);
        	idsProduto.push(2);
//			  for (var i = 0; i < self.selected.length; i++) { 
//				  idsCategorias.push(self.selected[i].id);
//			  }
			  var id = [];
			  id.push(1);
			  
			  $http({
      		method : 'POST',
              url: '/ponto/usePonto',
              data: {"id" : id, "idProduto":idsProduto}
              }).success(function(data) {    
            	  if(data==0){
            		  alert('Saldo insuficiente');
            	  }else {
            		  alert('Ok');
            	  }              	                    
              })
              .error(function(e) {
              	$mdDialog.show(
            		      $mdDialog.alert()
            		        .parent(angular.element(document.querySelector('#popupContainer')))
            		        .clickOutsideToClose(false)
            		        .title('Erro')
            		        .textContent('Os Pontos não foram usados. Ocorreu um erro.')
            		        .ariaLabel('Erro')
            		        .ok('Ok!')
            		    )
              });
		  }
        
        function iniciar() {        	
        	//getAlunos();   
        	getProdutos();
        	menuFactory.menu('produto',$rootScope);
        }
        
    	iniciar();
    	
	}
	
	function ProdutoDialogCtrl ($scope, $rootScope, $mdDialog) {
		  var $ctrl = this;
		  $ctrl.toggleAll = toggleAll;
		  $ctrl.exists = exists;
		  $ctrl.toggle = toggle;
		  
//		  $ctrl.items = alunos;
//		  $ctrl.selected = {
//		    item: $ctrl.items[0]
//		  };
		  $ctrl.selected = ['Aproveitamento Coletivo'];
		  
		  $ctrl.isIndeterminate = false;
		  $ctrl.isChecked = false;
		    
		  $ctrl.ok = function () {
		    $uibModalInstance.close($ctrl.selected.item);
		  };

		  $ctrl.cancel = function () {
		    $uibModalInstance.dismiss('cancel');
		  };
		  
		   function toggle(item, list) {
		        var idx = list.indexOf(item);
		        if (idx > -1) {
		          list.splice(idx, 1);
		        }
		        else {
		          list.push(item);
		        }
		      }
		    
		   function exists(item, list) {
		        return list.indexOf(item) > -1;
		      };

		      $ctrl.isIndeterminate = function() {
		        return ($ctrl.selected.length !== 0 &&
		        		$ctrl.selected.length !== $ctrl.items.length);
		      };

		      $ctrl.isChecked = function() {
		        return $ctrl.selected.length === $ctrl.items.length;
		      };

		      function toggleAll()  {
		        if ($ctrl.selected.length === $ctrl.items.length) {
		        	$ctrl.selected = [];
		        } else if ($ctrl.selected.length === 0 || $ctrl.selected.length > 0) {
		        	$ctrl.selected = $ctrl.items.slice(0);
		        }
		      };
		}
	
})(angular);