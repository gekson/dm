;
(function(angular) {
	'use strict';
	
	angular
		.module('app.produto')
        .config(route);
    
    route.$inject = ['$stateProvider'];
    
    function route($stateProvider) {
        $stateProvider
        
	        .state('listaProduto', {
	            url: '/listaProduto',
	            templateUrl: './views/produto/lista.html',
	            controller: 'ProdutoController',
                controllerAs: 'produtoCtrl'
	        })
        
        .state('cadastroProduto', {
            url: '/cadastroProduto',
            templateUrl: './views/produto/cadastro.html',
            controller: 'ProdutoController',            
            controllerAs: 'produtoCtrl'
        })
        .state('editProduto', {
            url: '/editProduto',
            templateUrl: './views/produto/edit.html',
            controller: 'ProdutoEditController',
            params:      {'produto': null},
            controllerAs: 'produtoEditCtrl'
        });
    }
	
})(angular);