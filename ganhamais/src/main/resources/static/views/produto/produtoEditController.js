;
(function(angular) {
	'use strict';
	
	angular
		.module('app.produto')
		.controller('ProdutoEditController', ProdutoEditController)
		.directive('chooseFile', function() {
    return {
      link: function (scope, elem, attrs) {
        var button = elem.find('button');
        var input = angular.element(elem[0].querySelector('input#fileInput'));
        button.bind('click', function() {
          input[0].click();
        });
        input.bind('change', function(e) {
          scope.$apply(function() {
            var files = e.target.files;
            if (files[0]) {
              scope.fileName = files[0].name;
              scope.fileImagem = files[0];
            } else {
              scope.fileName = null;
            }
          });
        });
      }
    };
  });
    
	ProdutoEditController.$inject = ['$q', '$location', '$http',  '$timeout', '$window', '$scope', '$rootScope', 'menuFactory', '$mdDialog', '$uibModal', '$state', 'Upload', '$stateParams'];
	
	function ProdutoEditController($q, $location, $http, $timeout, $window, $scope, $rootScope, menuFactory, $mdDialog, $uibModal, $state, Upload, $stateParams) {
		var vm = this;		                
        vm.alunos={};                
        vm.saveProduto = saveProduto;              
        vm.produto = $stateParams.produto;
        
        vm.teste = "self";        
        vm.incluiProdduto = {};
        vm.foto;
        
        
        function saveProduto(){
        	vm.incluiProdduto.produto = vm.produto;
        	
        	if($scope.fileImagem == null || $scope.fileImagem == undefined){
        		atualiza();
        	}else{
        		upload();
        	}        	
        }
        
        function upload() {
        	Upload.upload({
				url: '/produto/editProdutoComFoto',
				fields: {'nome': vm.produto.nome,
					'destaque': vm.produto.destaque},
				data: {arquivo: $scope.fileImagem,
					'nome': vm.produto.nome,
					'valor': vm.produto.valor,					
					'destaque': vm.produto.destaque,
					'descricao': vm.produto.descricao, 'id': vm.produto.id}
			}).success(function(data) {            						
            	$mdDialog.show(
            		      $mdDialog.alert()
            		        .parent(angular.element(document.querySelector('#popupContainer')))
            		        .clickOutsideToClose(false)
            		        .title('Sucesso')
            		        .textContent('Produto alterado com sucesso.')
            		        .ariaLabel('Sucesso')
            		        .ok('Ok!')
            		    ).then(function(answer) {
            		    	$state.go('listaProduto');
            	    	    });                    	
            })
            .error(function(e) {
            	$mdDialog.show(
          		      $mdDialog.alert()
          		        .parent(angular.element(document.querySelector('#popupContainer')))
          		        .clickOutsideToClose(false)
          		        .title('Erro')
          		        .textContent('O Produto não foi alterado. Ocorreu um erro.'+e.message)
          		        .ariaLabel('Erro')
          		        .ok('Ok!')
          		    )
            });
        }
         
        function atualiza() {
        	Upload.upload({
				url: '/produto/editProduto',
				fields: {'nome': vm.produto.nome,
					'destaque': vm.produto.destaque},
				data: {arquivo: $scope.fileImagem,
					'nome': vm.produto.nome,
					'valor': vm.produto.valor,					
					'destaque': vm.produto.destaque,
					'descricao': vm.produto.descricao, 'id': vm.produto.id}
			}).success(function(data) {            						
            	$mdDialog.show(
            		      $mdDialog.alert()
            		        .parent(angular.element(document.querySelector('#popupContainer')))
            		        .clickOutsideToClose(false)
            		        .title('Sucesso')
            		        .textContent('Produto alterado com sucesso.')
            		        .ariaLabel('Sucesso')
            		        .ok('Ok!')
            		    ).then(function(answer) {
            		    	$state.go('listaProduto');
            	    	    });                    	
            })
            .error(function(e) {
            	$mdDialog.show(
          		      $mdDialog.alert()
          		        .parent(angular.element(document.querySelector('#popupContainer')))
          		        .clickOutsideToClose(false)
          		        .title('Erro')
          		        .textContent('O Produto não foi alterado. Ocorreu um erro.'+e.message)
          		        .ariaLabel('Erro')
          		        .ok('Ok!')
          		    )
            });
        }
             
        function iniciar() {        	        	
        	menuFactory.menu('produto',$rootScope);
        }
        
    	iniciar();
    	
	}		
	
})(angular);