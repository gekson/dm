;
(function(angular) {
	'use strict';
	
	angular
		.module('app.login')
		.controller('LoginController', LoginController);
    
	LoginController.$inject = ['$q', '$location', '$http',  '$timeout', '$window', '$scope', '$rootScope'];
	
	function LoginController($q, $location, $http, $timeout, $window, $scope, $rootScope) {
		var vm = this;	
		vm.usuario;
		vm.senha;
		vm.login = login;
        
        function login(){        	
//        	$http({
//            		method : 'POST',
//                    url: '/validaUsuario',
//                    data: vm.usuario
//                    }).success(function(data) {
//                    	//vm.gridConfiguration.data = data;
//                        //swal({text: "Buscando informações...", type: "success", timer: 2000, showConfirmButton:false, allowOutsideClick:false});
//                        //vm.hubLink={};
//                    	if(data == true){
//                    		$window.location.href = '/index.html';	
//                    	}                    	
//                    })
//                    .error(function(e) {
//                    	//swal({text: "Nenhum HUB encontrado!", type: "warning"});
//                    });
        	$http({
        		method : 'POST',
                url: '/usuario/findUsuarioByLoginAndSenha',
                data: {"matricula" : vm.usuario, "senha":vm.senha}
                }).success(function(data) {                   	
                	if(data.id) {                		
                        $rootScope.authenticated = true;       
                        $location.path('/dashboard.html'); 
//                        $rootScope.authorites = data.authorites;
                    } else {
                        $rootScope.authenticated = false;
                        alert("Usuário ou senha inválido.");
                    }                	
                })
                .error(function(e) {
                	alert("Usuário ou senha inválido.");
                	$rootScope.authenticated = false;
                });
        }
        
        
        
        function iniciar() {        	
//        	var promises = [];
//        	promises.push($http.get('hub/cidades'));
        	        	
        }
        
    	iniciar();
    	
	}
	
})(angular);