;
(function(angular) {
	'use strict';
	
	angular
		.module('app.login')
        .config(route);
    
    route.$inject = ['$stateProvider'];
    
    function route($stateProvider) {
        $stateProvider
        
	        .state('login', {
	            url: '/login',
	            templateUrl: '/login.html',
	            data: {pageTitle: 'LOGIN'},
	            controller: 'LoginController',
                controllerAs: 'loginCtrl'
	        });
    }
	
})(angular);