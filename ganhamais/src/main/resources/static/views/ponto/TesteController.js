;
(function(angular) {
	'use strict';
	
	angular
		.module('app.ponto')
		.controller('TesteController', TesteController);
    
	TesteController.$inject = ['$q', '$location', '$http',  '$timeout', '$window', '$scope', '$rootScope', 'menuFactory', '$mdDialog'];
	
	function TesteController($q, $location, $http, $timeout, $window, $scope, $rootScope, menuFactory, $mdDialog) {
		var self = this;

	    self.simulateQuery = false;
	    self.isDisabled    = false;

	    // list of `state` value/display objects
	    self.states        = loadAll();
	    self.querySearch   = querySearch;
	    self.selectedItemChange = selectedItemChange;
	    self.searchTextChange   = searchTextChange;

	    self.newState = newState;
	    
	    self.items = [1,2,3,4,5];
	    self.selected = [1];
	    self.isIndeterminate = false;
	    self.isChecked = false;
	    
	    self.toggle = toggle;
	    self.exists = exists;
	    self.toggleAll = toggleAll;
	    
	    self.addPontosTurma = addPontosTurma;
	    self.getAlunosTurma = getAlunosTurma;	    
	    self.listaAlunos = [];
	    
	    menuFactory.menu('ponto',$rootScope);
	    
	    self.toppings = [
	                       { name: 'Pepperoni', wanted: true },
	                       { name: 'Sausage', wanted: false },
	                       { name: 'Black Olives', wanted: true },
	                       { name: 'Green Peppers', wanted: false }
	                     ];

	    function addPontosTurma(ev) {
	    	$mdDialog.show({
	    	      controller: TesteController,
	    	      templateUrl: './views/ponto/ponto_turma.html',
	    	      parent: angular.element(document.body),
	    	      targetEvent: ev,
	    	      clickOutsideToClose:true,
	    	      fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
	    	    })
	    	    .then(function(answer) {
	    	      $scope.status = 'You said the information was "' + answer + '".';
	    	    }, function() {
	    	      $scope.status = 'You cancelled the dialog.';
	    	    });
	    }
	    
	    function getAlunosTurma(topping, event) {
	    	self.listaAlunos = [topping.name];
	    	self.listaAlunos = [
	   	                     { name: 'Janet Perkins', img: 'img/100-0.jpeg', newMessage: true },
	   	                     { name: 'Mary Johnson', img: 'img/100-1.jpeg', newMessage: false },
	   	                     { name: 'Peter Carlsson', img: 'img/100-2.jpeg', newMessage: false }
	   	                   ];
	    }
	    
	    function toggle(item, list) {
	        var idx = list.indexOf(item);
	        if (idx > -1) {
	          list.splice(idx, 1);
	        }
	        else {
	          list.push(item);
	        }
	      }
	    
	    function exists(item, list) {
	        return list.indexOf(item) > -1;
	      };

	      self.isIndeterminate = function() {
	        return (self.selected.length !== 0 &&
	        		self.selected.length !== self.items.length);
	      };

	      self.isChecked = function() {
	        return self.selected.length === self.items.length;
	      };

	      function toggleAll() {
	        if (self.selected.length === self.items.length) {
	        	self.selected = [];
	        } else if (self.selected.length === 0 || self.selected.length > 0) {
	        	self.selected = self.items.slice(0);
	        }
	      };
	    
	    function newState(state) {
	      alert("Sorry! You'll need to create a Constitution for " + state + " first!");
	    }

	    // ******************************
	    // Internal methods
	    // ******************************

	    /**
	     * Search for states... use $timeout to simulate
	     * remote dataservice call.
	     */
	    function querySearch (query) {
	      var results = query ? self.states.filter( createFilterFor(query) ) : self.states,
	          deferred;
	      if (self.simulateQuery) {
	        deferred = $q.defer();
	        $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
	        return deferred.promise;
	      } else {
	        return results;
	      }
	    }

	    function searchTextChange(text) {
	      $log.info('Text changed to ' + text);
	    }

	    function selectedItemChange(item) {
	      $log.info('Item changed to ' + JSON.stringify(item));
	    }

	    /**
	     * Build `states` list of key/value pairs
	     */
	    function loadAll() {
	      var allStates = 'Alabama, Alaska, Arizona, Arkansas, California, Colorado, Connecticut, Delaware,\
	              Florida, Georgia, Hawaii, Idaho, Illinois, Indiana, Iowa, Kansas, Kentucky, Louisiana,\
	              Maine, Maryland, Massachusetts, Michigan, Minnesota, Mississippi, Missouri, Montana,\
	              Nebraska, Nevada, New Hampshire, New Jersey, New Mexico, New York, North Carolina,\
	              North Dakota, Ohio, Oklahoma, Oregon, Pennsylvania, Rhode Island, South Carolina,\
	              South Dakota, Tennessee, Texas, Utah, Vermont, Virginia, Washington, West Virginia,\
	              Wisconsin, Wyoming';

	      return allStates.split(/, +/g).map( function (state) {
	        return {
	          value: state.toLowerCase(),
	          display: state
	        };
	      });
	    }

	    /**
	     * Create filter function for a query string
	     */
	    function createFilterFor(query) {
	      var lowercaseQuery = angular.lowercase(query);

	      return function filterFn(state) {
	        return (state.value.indexOf(lowercaseQuery) === 0);
	      };
	    }
    	
	}
	
})(angular);