;
(function(angular) {
	'use strict';
	
	angular
		.module('app.ponto')
        .config(route);
    
    route.$inject = ['$stateProvider'];
    
    function route($stateProvider) {
        $stateProvider
        
	        .state('cadastroPonto', {
	            url: '/cadastroPonto',
	            templateUrl: './views/ponto/cadastro.html',	            
	            controller: 'PontoController',
                controllerAs: 'pontoCtrl'
	        })
	        
	        .state('cadastroPontoAluno', {
	            url: '/cadastroPontoAluno',
	            templateUrl: './views/ponto/ponto_aluno.html',	            
	            controller: 'PontoAlunoController',
                controllerAs: 'pontoAlunoCtrl'
	        });
    }
	
})(angular);