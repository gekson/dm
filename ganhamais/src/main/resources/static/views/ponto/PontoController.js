;
(function(angular) {
	'use strict';
	
	angular
		.module('app.ponto')
		.controller('PontoController', PontoController)
		.controller('ModalInstanceCtrl', ModalInstanceCtrl);

	PontoController.$inject = ['$q', '$location', '$http',  '$timeout', '$window', '$scope', '$rootScope', 'menuFactory', '$mdDialog', '$uibModal'];
	
	function PontoController($q, $location, $http, $timeout, $window, $scope, $rootScope, menuFactory, $mdDialog, $uibModal) {
		var self = this;

	    self.simulateQuery = false;
	    self.isDisabled    = false;

	    // list of `state` value/display objects
	    self.states        = loadAll();
	    self.querySearch   = querySearch;
	    self.selectedItemChange = selectedItemChange;
	    self.searchTextChange   = searchTextChange;

	    self.newState = newState;
	    
	    self.items = ['Aproveitamento Coletivo','Disciplina Coletiva','Disciplina Coletiva Organizacional'];
	    self.selected = ['Aproveitamento Coletivo'];
	    self.isIndeterminate = false;
	    self.isChecked = false;
	    
	    self.toggle = toggle;
	    self.exists = exists;
	    self.toggleAll = toggleAll;
	    
	    self.addPontosTurma = addPontosTurma;
	    self.getAlunosTurma = getAlunosTurma;	    
	    self.addPontosAluno = addPontosAluno;
	    self.getTurmas = getTurmas();
	    self.listaAlunos = [];
	    self.open = open;
	    self.mostraAluno = false;
	    
	    menuFactory.menu('ponto',$rootScope);
	    
	    self.toppings = [
	                       { name: '4 Ano A', wanted: true },
	                       { name: '4 Ano B', wanted: false },
	                       { name: '1 Ano Médio', wanted: true },
	                       { name: '3 Ano Médio', wanted: false }
	                     ];
	    self.turmas = {};
	    self.turmasSelecionadas = [];
	    
	    function getTurmas(){
        	$http
            .get('/turma')
                .success(function(data) {
                    self.turmas = data;
                });  
        }
	    
	    function verificaChecked(){	    	
	    	self.turmas.forEach(function (value) {
				if (value.checked){
					self.turmasSelecionadas.push(value.id);
			}
				
			});
	    }

	    function open(size) {
	        var modalInstance = $uibModal.open({
	            animation: true,
	            ariaLabelledBy: 'modal-title',
	            ariaDescribedBy: 'modal-body',
	            templateUrl: 'myModalContent.html',
	            controller: 'ModalInstanceCtrl',
	            controllerAs: '$ctrl',
	            size: size,
	            resolve: {
	              items: function () {
	                return self.items;
	              }
	            }
	          });
	    }
	    function addPontosTurma(ev) {
	    	verificaChecked();
	    	$mdDialog.show({
	    		controller: DialogCtrl,
	    		controllerAs: 'dialogCtrl',
	    	      templateUrl: './views/ponto/teste.html',
	    	      parent: angular.element(document.body),
	    	      targetEvent: ev,
	    	      clickOutsideToClose:true,
	    	      fullscreen: true, // Only for -xs, -sm breakpoints.
	    	      locals : {
		              ids:  self.turmasSelecionadas
		            }
	    	    })
	    	    .then(function(answer) {
	    	      $scope.status = 'You said the information was "' + answer + '".';
	    	    }, function() {
	    	      $scope.status = 'You cancelled the dialog.';
	    	      self.turmasSelecionadas = [];
	    	    });
	    }
	    
	    function addPontosAluno(ev) {
	    	$mdDialog.show({
	    		controller: AlunoDialogCtrl,
	    		controllerAs: 'alunoDialogCtrl',
	    	      templateUrl: './views/ponto/ponto_aluno.html',
	    	      parent: angular.element(document.body),
	    	      targetEvent: ev,
	    	      clickOutsideToClose:true,
	    	      fullscreen: true, // Only for -xs, -sm breakpoints.
	    	      locals : {
		              alunos:  self.listaAlunos
		            }
	    	    })
	    	    .then(function(answer) {
	    	      $scope.status = 'You said the information was "' + answer + '".';
	    	    }, function() {
	    	      $scope.status = 'You cancelled the dialog.';
	    	    });
	    }
	    
	    function getAlunosTurma(topping, event) {
	    	self.listaAlunos = [topping.name];
	    	self.listaAlunos = [
	   	                     { name: 'Janet Perkins', img: '/assets/img/faces/marc.jpg', newMessage: true },
	   	                     { name: 'Mary Johnson', img: '/assets/img/faces/marc.jpg', newMessage: false },
	   	                     { name: 'Peter Carlsson', img: '/assets/img/faces/marc.jpg', newMessage: false }
	   	                   ];
	    }
	    
	    function toggle(item, list) {
	        var idx = list.indexOf(item);
	        if (idx > -1) {
	          list.splice(idx, 1);
	        }
	        else {
	          list.push(item);
	        }
	      }
	    
	    function exists(item, list) {
	        return list.indexOf(item) > -1;
	      };

	      self.isIndeterminate = function() {
	        return (self.selected.length !== 0 &&
	        		self.selected.length !== self.items.length);
	      };

	      self.isChecked = function() {
	        return self.selected.length === self.items.length;
	      };

	      function toggleAll() {
	        if (self.selected.length === self.items.length) {
	        	self.selected = [];
	        } else if (self.selected.length === 0 || self.selected.length > 0) {
	        	self.selected = self.items.slice(0);
	        }
	      };
	    
	    function newState(state) {
	      alert("Sorry! You'll need to create a Constitution for " + state + " first!");
	    }

	    // ******************************
	    // Internal methods
	    // ******************************

	    /**
	     * Search for states... use $timeout to simulate
	     * remote dataservice call.
	     */
	    function querySearch (query) {
	      var results = query ? self.states.filter( createFilterFor(query) ) : self.states,
	          deferred;
	      if (self.simulateQuery) {
	        deferred = $q.defer();
	        $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
	        return deferred.promise;
	      } else {
	        return results;
	      }
	    }

	    function searchTextChange(text) {
	      $log.info('Text changed to ' + text);
	    }

	    function selectedItemChange(item) {
	      $log.info('Item changed to ' + JSON.stringify(item));
	    }

	    /**
	     * Build `states` list of key/value pairs
	     */
	    function loadAll() {
	      var allStates = 'Alabama, Alaska, Arizona, Arkansas, California, Colorado, Connecticut, Delaware,\
	              Florida, Georgia, Hawaii, Idaho, Illinois, Indiana, Iowa, Kansas, Kentucky, Louisiana,\
	              Maine, Maryland, Massachusetts, Michigan, Minnesota, Mississippi, Missouri, Montana,\
	              Nebraska, Nevada, New Hampshire, New Jersey, New Mexico, New York, North Carolina,\
	              North Dakota, Ohio, Oklahoma, Oregon, Pennsylvania, Rhode Island, South Carolina,\
	              South Dakota, Tennessee, Texas, Utah, Vermont, Virginia, Washington, West Virginia,\
	              Wisconsin, Wyoming';

	      return allStates.split(/, +/g).map( function (state) {
	        return {
	          value: state.toLowerCase(),
	          display: state
	        };
	      });
	    }

	    /**
	     * Create filter function for a query string
	     */
	    function createFilterFor(query) {
	      var lowercaseQuery = angular.lowercase(query);

	      return function filterFn(state) {
	        return (state.value.indexOf(lowercaseQuery) === 0);
	      };
	    }
    	
	}
	
	function ModalInstanceCtrl ($uibModalInstance, items) {
		  var $ctrl = this;
		  $ctrl.toggleAll = toggleAll;
		  $ctrl.exists = exists;
		  $ctrl.toggle = toggle;
		  
		  $ctrl.items = items;
//		  $ctrl.selected = {
//		    item: $ctrl.items[0]
//		  };
		  $ctrl.selected = [];
		  
		  $ctrl.isIndeterminate = false;
		  $ctrl.isChecked = false;
		    
		  $ctrl.ok = function () {
		    $uibModalInstance.close($ctrl.selected.item);
		  };

		  $ctrl.cancel = function () {
		    $uibModalInstance.dismiss('cancel');
		  };
		  
		   function toggle(item, list) {
		        var idx = list.indexOf(item);
		        if (idx > -1) {
		          list.splice(idx, 1);
		        }
		        else {
		          list.push(item);
		        }
		      }
		    
		   function exists(item, list) {
		        return list.indexOf(item) > -1;
		      };

		      $ctrl.isIndeterminate = function() {
		        return ($ctrl.selected.length !== 0 &&
		        		$ctrl.selected.length !== $ctrl.items.length);
		      };

		      $ctrl.isChecked = function() {
		        return $ctrl.selected.length === $ctrl.items.length;
		      };

		      function toggleAll()  {
		        if ($ctrl.selected.length === $ctrl.items.length) {
		        	$ctrl.selected = [];
		        } else if ($ctrl.selected.length === 0 || $ctrl.selected.length > 0) {
		        	$ctrl.selected = $ctrl.items.slice(0);
		        }
		      };
		}
	
	function DialogCtrl ($scope, $rootScope, $mdDialog, $http, ids) {
		  var $ctrl = this;
		  $ctrl.toggleAll = toggleAll;
		  $ctrl.exists = exists;
		  $ctrl.toggle = toggle;
		  $ctrl.pontuar = pontuar;
		  $ctrl.getCategorias = getCategorias();			  
		  
//		  $ctrl.items = self.items = ['Aproveitamento Coletivo','Disciplina Coletiva','Disciplina Coletiva Organizacional'];
//		  $ctrl.selected = {
//		    item: $ctrl.items[0]
//		  };
		  $ctrl.selected = [];
		  
		  $ctrl.isIndeterminate = false;
		  $ctrl.isChecked = false;
		  $ctrl.pontos = {};
		  $ctrl.unidade;
		  $ctrl.mes;
		  $ctrl.categoria;
		  $ctrl.porUnidade = false;
		  $ctrl.pontoVariavel = false;
		  $ctrl.ponto;
		  $ctrl.categorias = [];
		  $ctrl.showCategorias = false;
		  $ctrl.addCategoria = addCategoria;
		  $ctrl.verificaCategoria = verificaCategoria;
		  $ctrl.excluiCategoria = excluiCategoria;
		  
		  $ctrl.meses = ('Janeiro Fevereiro Março Abril Maio Junho Julho Agosto Setembro Outubro Novembro Dezembro')
      		.split(' ').map(function (meses) { return { nome: meses }; });       
		    
		  $ctrl.ok = function () {
		    $uibModalInstance.close($ctrl.selected.item);
		  };

		  $ctrl.cancel = function () {
		    $uibModalInstance.dismiss('cancel');
		  };
		  
		  function getCategorias(){
	        	$http
	            .get('/ponto/getCategoriasColetivo')
	                .success(function(data) {
	                	$ctrl.items = data;
	                });  
	        }
		  
		  function verificaCategoria() {
			  $ctrl.porUnidade = false;
			  if($ctrl.categoria.porUnidade) {
				  $ctrl.porUnidade = true;
			  }
			  
			  $ctrl.pontoVariavel = false;
			  if($ctrl.categoria.pontoVariavel) {
				  $ctrl.pontoVariavel = true;
			  }
			  $ctrl.unidade=null;
			  $ctrl.mes=null;
			  $ctrl.ponto=null;
		  }
		  
		  function addCategoria() {			  
			  if($ctrl.categoria != null && ($ctrl.unidade != null || $ctrl.mes != null) ){
				  if($ctrl.categoria.pontoVariavel){
					  $ctrl.categoria.pontos = $ctrl.ponto;
				  }
				  if($ctrl.mes!=null) {
					  $ctrl.categoria.mesUnidade = $ctrl.mes;  
				  }else{
					  $ctrl.categoria.mesUnidade = $ctrl.unidade;
				  }
				  $ctrl.categorias.push($ctrl.categoria);
				  $ctrl.showCategorias = true;
				  
			  }else{
				  alert('Preencha a Categoria ou Mês/Unidade.');
			  }
		  }
		  
		  function excluiCategoria(item) {
			  var index = $ctrl.categorias.indexOf(item);
			  $ctrl.categorias.splice(index, 1);
		  }
		  
		  function pontuar() {
//			  var idsCategorias = [];
//			  for (var i = 0; i < $ctrl.selected.length; i++) { 
//				  idsCategorias.push($ctrl.selected[i].id);
//			  }
//			  var unidade = [];
//			  unidade.push($ctrl.unidade);
//			  
			  var idUsuario = [];
			  idUsuario.push($rootScope.usuarioLogado.id);
			  
//			  var mes = [];
//			  mes.push($ctrl.mes);
//			  
			  $http({
          		method : 'POST',
                  url: '/ponto/addPontoTurma',
                  //data: {[vm.produto, $scope.fileName]}
                  data: {"categorias" : $ctrl.categorias, "ids":ids, "idUsuario":idUsuario}
                  }).success(function(data) {
                  	//vm.alunos = data;
                      //swal({text: "Buscando informações...", type: "success", timer: 2000, showConfirmButton:false, allowOutsideClick:false});
                      //vm.hubLink={};
                  	$mdDialog.show(
                  		      $mdDialog.alert()
                  		        .parent(angular.element(document.querySelector('#popupContainer')))
                  		        .clickOutsideToClose(false)
                  		        .title('Sucesso')
                  		        .textContent('Pontos cadastrados com sucesso.')
                  		        .ariaLabel('Sucesso')
                  		        .ok('Ok!')
                  		    ).then(function(answer) {
                  		    	//$state.go('listaProduto');
                  	    	    });                    	
                  })
                  .error(function(e) {
                  	$mdDialog.show(
                		      $mdDialog.alert()
                		        .parent(angular.element(document.querySelector('#popupContainer')))
                		        .clickOutsideToClose(false)
                		        .title('Erro')
                		        .textContent('Os Pontos não foram cadastrados. Ocorreu um erro.')
                		        .ariaLabel('Erro')
                		        .ok('Ok!')
                		    )
                  });
		  }
		   function toggle(item, list) {
		        var idx = list.indexOf(item);
		        if (idx > -1) {
		          list.splice(idx, 1);
		        }
		        else {
		          list.push(item);
		        }
		      }
		    
		   function exists(item, list) {
		        return list.indexOf(item) > -1;
		      };

		      $ctrl.isIndeterminate = function() {
		        return ($ctrl.selected.length !== 0 &&
		        		$ctrl.selected.length !== $ctrl.items.length);
		      };

		      $ctrl.isChecked = function() {
//		        return $ctrl.selected.length === $ctrl.items.length;
		      };

		      function toggleAll()  {
		        if ($ctrl.selected.length === $ctrl.items.length) {
		        	$ctrl.selected = [];
		        } else if ($ctrl.selected.length === 0 || $ctrl.selected.length > 0) {
		        	$ctrl.selected = $ctrl.items.slice(0);
		        }
		      };
		}
	
	function AlunoDialogCtrl ($scope, $rootScope, $mdDialog, alunos) {
		  var $ctrl = this;
		  $ctrl.toggleAll = toggleAll;
		  $ctrl.exists = exists;
		  $ctrl.toggle = toggle;
		  
		  $ctrl.items = alunos;
//		  $ctrl.selected = {
//		    item: $ctrl.items[0]
//		  };
		  $ctrl.selected = ['Aproveitamento Coletivo'];
		  
		  $ctrl.isIndeterminate = false;
		  $ctrl.isChecked = false;
		    
		  $ctrl.ok = function () {
		    $uibModalInstance.close($ctrl.selected.item);
		  };

		  $ctrl.cancel = function () {
		    $uibModalInstance.dismiss('cancel');
		  };
		  
		   function toggle(item, list) {
		        var idx = list.indexOf(item);
		        if (idx > -1) {
		          list.splice(idx, 1);
		        }
		        else {
		          list.push(item);
		        }
		      }
		    
		   function exists(item, list) {
		        return list.indexOf(item) > -1;
		      };

		      $ctrl.isIndeterminate = function() {
		        return ($ctrl.selected.length !== 0 &&
		        		$ctrl.selected.length !== $ctrl.items.length);
		      };

		      $ctrl.isChecked = function() {
		        return $ctrl.selected.length === $ctrl.items.length;
		      };

		      function toggleAll()  {
		        if ($ctrl.selected.length === $ctrl.items.length) {
		        	$ctrl.selected = [];
		        } else if ($ctrl.selected.length === 0 || $ctrl.selected.length > 0) {
		        	$ctrl.selected = $ctrl.items.slice(0);
		        }
		      };
		}
	
})(angular);