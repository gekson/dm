;
(function(angular) {
	'use strict';
	
	angular
		.module('app.ponto')
		.controller('PontoAlunoController', PontoAlunoController);

	PontoAlunoController.$inject = ['$q', '$location', '$http',  '$timeout', '$window', '$scope', '$rootScope', 'menuFactory', '$mdDialog'];
	
	function PontoAlunoController($q, $location, $http, $timeout, $window, $scope, $rootScope, menuFactory, $mdDialog) {
		var self = this;

	    self.simulateQuery = false;
	    self.isDisabled    = false;

	    // list of `state` value/display objects
	    self.states        = loadAll();
	    self.querySearch   = querySearch;
	    self.selectedItemChange = selectedItemChange;
	    self.searchTextChange   = searchTextChange;

	    self.newState = newState;
	    
	    self.items = ['Aproveitamento Coletivo','Disciplina Coletiva','Disciplina Coletiva Organizacional'];
	    self.selected = [];
	    self.isIndeterminate = false;
	    self.isChecked = false;
	    
	    self.toggle = toggle;
	    self.exists = exists;
	    self.toggleAll = toggleAll;
	    self.getCategoriasIndividual = getCategoriasIndividual();
	    self.pontuar = pontuar;
	    
	    self.buscaAluno = buscaAluno;	    	    
	    self.mostraAluno = false;
	    self.unidade;
	    self.mes;
	    
	    menuFactory.menu('pontoAluno',$rootScope);	  
	    
	    self.aluno = {
                nome: "",
                matricula: "",
                id: 0
            };
	    
	    self.alunos = [];
	    
	    self.meses = ('Janeiro Fevereiro Março Abril Maio Junho Julho Agosto Setembro Outubro Novembro Dezembro')
  		.split(' ').map(function (meses) { return { nome: meses }; });   
	    
	    function getCategoriasIndividual(){
        	$http
            .get('/ponto/getCategoriasIndividual')
                .success(function(data) {
                	self.items = data;
                });  
        }
	    
	    function buscaAluno() {
	    	$http({
        		method : 'POST',
                url: '/aluno/findAlunoByMatricula',
                //data: {vm.produto, vm.upload}
                data: self.aluno
                }).success(function(data) {
                	//vm.alunos = data;
                    //swal({text: "Buscando informações...", type: "success", timer: 2000, showConfirmButton:false, allowOutsideClick:false});
                    //vm.hubLink={};
                	self.alunos = data;
                	self.mostraAluno = true;                	
                })
                .error(function(e) {
                	$mdDialog.show(
              		      $mdDialog.alert()
              		        .parent(angular.element(document.querySelector('#pontosAluno')))
              		        .clickOutsideToClose(false)
              		        .title('Erro')
              		        .textContent('Aluno não cadastrado')
              		        .ariaLabel('Erro')
              		        .ok('Ok!')
              		    )
                });
	    }
	    
	    function pontuar() {
			  var idsCategorias = [];
			  for (var i = 0; i < self.selected.length; i++) { 
				  idsCategorias.push(self.selected[i].id);
			  }
			  var id = [];
			  id.push(self.alunos[0].id);
			  
			  var idUsuario = [];
			  idUsuario.push($rootScope.usuarioLogado.id);
			  
			  var unidade = [];
			  unidade.push(self.unidade);
			  
			  var mes = [];
			  mes.push(self.mes);
			  $http({
        		method : 'POST',
                url: '/ponto/addPontoAluno',
                //data: {[vm.produto, $scope.fileName]}
                data: {"categorias" : idsCategorias, "id":id, "idUsuario":idUsuario, "unidade":unidade, "mes":mes}
                }).success(function(data) {
                	//vm.alunos = data;
                    //swal({text: "Buscando informações...", type: "success", timer: 2000, showConfirmButton:false, allowOutsideClick:false});
                    //vm.hubLink={};
                	$mdDialog.show(
                		      $mdDialog.alert()
                		        .parent(angular.element(document.querySelector('#popupContainer')))
                		        .clickOutsideToClose(false)
                		        .title('Sucesso')
                		        .textContent('Pontos cadastrados com sucesso.')
                		        .ariaLabel('Sucesso')
                		        .ok('Ok!')
                		    ).then(function(answer) {
                		    	//$state.go('listaProduto');
                	    	    });                    	
                })
                .error(function(e) {
                	$mdDialog.show(
              		      $mdDialog.alert()
              		        .parent(angular.element(document.querySelector('#popupContainer')))
              		        .clickOutsideToClose(false)
              		        .title('Erro')
              		        .textContent('Os Pontos não foram cadastrados. Ocorreu um erro.')
              		        .ariaLabel('Erro')
              		        .ok('Ok!')
              		    )
                });
		  }
	    
	    
	    function toggle(item, list) {
	        var idx = list.indexOf(item);
	        if (idx > -1) {
	          list.splice(idx, 1);
	        }
	        else {
	          list.push(item);
	        }
	      }
	    
	    function exists(item, list) {
	        return list.indexOf(item) > -1;
	      };

	      self.isIndeterminate = function() {
	        return (self.selected.length !== 0 &&
	        		self.selected.length !== self.items.length);
	      };

	      self.isChecked = function() {
	        return self.selected.length === self.items.length;
	      };

	      function toggleAll() {
	        if (self.selected.length === self.items.length) {
	        	self.selected = [];
	        } else if (self.selected.length === 0 || self.selected.length > 0) {
	        	self.selected = self.items.slice(0);
	        }
	      };
	    
	    function newState(state) {
	      alert("Sorry! You'll need to create a Constitution for " + state + " first!");
	    }

	    // ******************************
	    // Internal methods
	    // ******************************

	    /**
	     * Search for states... use $timeout to simulate
	     * remote dataservice call.
	     */
	    function querySearch (query) {
	      var results = query ? self.states.filter( createFilterFor(query) ) : self.states,
	          deferred;
	      if (self.simulateQuery) {
	        deferred = $q.defer();
	        $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
	        return deferred.promise;
	      } else {
	        return results;
	      }
	    }

	    function searchTextChange(text) {
	      $log.info('Text changed to ' + text);
	    }

	    function selectedItemChange(item) {
	      $log.info('Item changed to ' + JSON.stringify(item));
	    }

	    /**
	     * Build `states` list of key/value pairs
	     */
	    function loadAll() {
	      var allStates = 'Alabama, Alaska, Arizona, Arkansas, California, Colorado, Connecticut, Delaware,\
	              Florida, Georgia, Hawaii, Idaho, Illinois, Indiana, Iowa, Kansas, Kentucky, Louisiana,\
	              Maine, Maryland, Massachusetts, Michigan, Minnesota, Mississippi, Missouri, Montana,\
	              Nebraska, Nevada, New Hampshire, New Jersey, New Mexico, New York, North Carolina,\
	              North Dakota, Ohio, Oklahoma, Oregon, Pennsylvania, Rhode Island, South Carolina,\
	              South Dakota, Tennessee, Texas, Utah, Vermont, Virginia, Washington, West Virginia,\
	              Wisconsin, Wyoming';

	      return allStates.split(/, +/g).map( function (state) {
	        return {
	          value: state.toLowerCase(),
	          display: state
	        };
	      });
	    }

	    /**
	     * Create filter function for a query string
	     */
	    function createFilterFor(query) {
	      var lowercaseQuery = angular.lowercase(query);

	      return function filterFn(state) {
	        return (state.value.indexOf(lowercaseQuery) === 0);
	      };
	    }
    	
	}
		
	
})(angular);