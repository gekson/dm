;
(function(angular) {
	'use strict';
	
	angular
		.module('app.usuario')
		.controller('UsuarioController', UsuarioController);
    
	UsuarioController.$inject = ['$q', '$location', '$http',  '$timeout', '$window', '$scope', '$rootScope', 'menuFactory', '$mdDialog', '$state'];
	
	function UsuarioController($q, $location, $http, $timeout, $window, $scope, $rootScope, menuFactory, $mdDialog, $state) {
		var vm = this;		                
        vm.usuarios={};
        vm.getUsuarios = getUsuarios;   
        vm.saveUsuario = saveUsuario;
                
        vm.user = {
        		login: "",
        		senha: ""
        };        
        
        function getUsuarios(){        	
        	$http({
            		method : 'GET',
                    url: '/usuario/getUsuarios'                    
                    }).success(function(data) {                    	
                    	vm.usuarios = data;                                             	
                    })
                    .error(function(e) {                    	                  	
                    });
        } 
        

        function iniciar() {        	
        	getUsuarios();    
        	menuFactory.menu('usuario',$rootScope);
        }                
        
        function saveUsuario(){
        	$http({
        		method : 'POST',
                url: '/usuario/criaUsuario',
                data: {"login":vm.user.login, "senha":vm.user.senha}
                }).success(function(data) {  
                	$mdDialog.show(
                		      $mdDialog.alert()
                		        .parent(angular.element(document.querySelector('#usuarios')))
                		        .clickOutsideToClose(false)
                		        .title('Sucesso')
                		        .textContent('Usuário cadastrado com sucesso.')
                		        .ariaLabel('Sucesso')
                		        .ok('Ok!')
                		    )
                	$state.go('listaUsuario');
                })
                .error(function(e) {
                	$mdDialog.show(
              		      $mdDialog.alert()
              		        .parent(angular.element(document.querySelector('#usuarios')))
              		        .clickOutsideToClose(false)
              		        .title('Erro')
              		        .textContent('Erro ao cadastrar usuário.' +e.message)
              		        .ariaLabel('Erro')
              		        .ok('Ok!')
              		    )
                });
        }                
        
    	iniciar();
    	
	}
	
})(angular);