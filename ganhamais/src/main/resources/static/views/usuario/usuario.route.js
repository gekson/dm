;
(function(angular) {
	'use strict';
	
	angular
		.module('app.usuario')
        .config(route);
    
    route.$inject = ['$stateProvider'];
    
    function route($stateProvider) {
        $stateProvider
        	        
	        .state('listaUsuario', {
	            url: '/listaUsuario',
	            templateUrl: './views/usuario/lista.html',
	            data: {pageTitle: 'LISTA DE USUÁRIOS'},
	            controller: 'UsuarioController',
                controllerAs: 'usuarioCtrl'
	        })
	        .state('cadastroUsuario', {
            url: '/cadastroUsuario',
            templateUrl: './views/usuario/cadastro.html',
            controller: 'UsuarioController',
            controllerAs: 'usuarioCtrl'
	        });
    }
	
})(angular);