package br.com.divinomestre.ganhamais.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.divinomestre.ganhamais.models.Aluno;
import br.com.divinomestre.ganhamais.models.Categoria;
import br.com.divinomestre.ganhamais.models.Ponto;
import br.com.divinomestre.ganhamais.models.Turma;
import br.com.divinomestre.ganhamais.models.Usuario;
import br.com.divinomestre.ganhamais.repository.AlunoRepository;
import br.com.divinomestre.ganhamais.repository.CategoriaRepository;
import br.com.divinomestre.ganhamais.repository.TurmaRepository;
import br.com.divinomestre.ganhamais.repository.UsuarioRepository;

@Service
public class TurmaService extends AbstractService<Turma, Long> {

	@Autowired
	private TurmaRepository turmaRepository;

	@Autowired
	private CategoriaRepository categoriaRepository;

	@Autowired
	private AlunoRepository alunoRepository;

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Override
	protected JpaRepository<Turma, Long> getRepository() {
		return turmaRepository;
	}

	public List<Turma> findByIds(Collection<Long> ids) {
		return turmaRepository.findAll(ids);
	}

	@Transactional
	public void addPontoTurma(List<Turma> turmas, List<Long> idsCategorias, Long idUsuario, Integer unidade,
			String mes) {
		List<Categoria> categorias = categoriaRepository.findAll(idsCategorias);
		List<Ponto> pontos = new ArrayList<Ponto>();
		Usuario usuario = (Usuario) this.usuarioRepository.getOne(idUsuario);
		for (Turma turma : turmas) {
			for (Aluno aluno : turma.getAlunos()) {
				for (Categoria categoria : categorias) {
					Ponto ponto = new Ponto();
					ponto.setData(new Date());
					ponto.setAno(Integer.toString(LocalDate.now().getYear()));
					ponto.setCategoria(categoria);
					ponto.setQuantidade(categoria.getPontos());
					ponto.setTipo("C");
					ponto.setTurma(turma);
					ponto.setAluno(aluno);
					ponto.setResponsavel(usuario);
					ponto.setUnidade(unidade);
					ponto.setMes(mes);
					pontos.add(ponto);
				}
				aluno.setPontos(pontos);
				alunoRepository.save(aluno);
			}
		}

	}

	public List<Categoria> getCategoriasColetivo() {
		return categoriaRepository.findByColetivo(true);
	}

	public List<Categoria> getCategoriasIndividual() {
		return categoriaRepository.findByColetivo(false);
	}

}
