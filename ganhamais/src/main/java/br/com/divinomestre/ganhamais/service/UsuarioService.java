package br.com.divinomestre.ganhamais.service;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import br.com.divinomestre.ganhamais.models.Usuario;
import br.com.divinomestre.ganhamais.repository.UsuarioRepository;

@Service
public class UsuarioService extends AbstractService<Usuario, Long> {

	@Autowired
	private UsuarioRepository usuarioRepository;
	@Autowired
	private UtilService utilService;

	@Override
	protected JpaRepository<Usuario, Long> getRepository() {
		return usuarioRepository;
	}

	public Usuario findUsuarioByLoginAndSenha(String usuario, String senha)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		return usuarioRepository.findUsuarioByLoginAndSenha(usuario, utilService.criptografar(senha));
	}

	public Usuario findUsuarioByLogin(String login) {
		return this.usuarioRepository.findUsuarioByLogin(login);
	}

	public void criaUsuario(String login, String senha) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		Usuario usuario = new Usuario();
		usuario.setLogin(login);
		usuario.setSenha(this.utilService.criptografar(senha));
		usuario.setIsAluno(Boolean.valueOf(false));

		this.usuarioRepository.save(usuario);
	}

	public List<Usuario> findUsuarioByIsAluno(Boolean aluno) {
		return this.usuarioRepository.findUsuarioByIsAluno(aluno);
	}
}
