package br.com.divinomestre.ganhamais.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.com.divinomestre.ganhamais.models.Produto;
import br.com.divinomestre.ganhamais.service.AbstractService;
import br.com.divinomestre.ganhamais.service.ProdutoService;
import br.com.divinomestre.ganhamais.service.UtilService;

@RestController
@CrossOrigin(origins = { "*" })
@RequestMapping({ "/produto" })
public class ProdutoController extends AbstractController<Produto, Long> {
	@Autowired
	private ProdutoService produtoService;
	@Autowired
	private UtilService utilService;

	protected AbstractService<Produto, Long> getService() {
		return this.produtoService;
	}

	@RequestMapping(value = { "salvarFoto" }, method = { RequestMethod.POST })
	public ResponseEntity<?> salvarFoto(@RequestParam("arquivo") MultipartFile file) {
		return ResponseEntity.ok().build();
	}

	@RequestMapping(value = { "salvarProduto" }, method = {
			RequestMethod.POST })
	public ResponseEntity<?> salvarProduto(@RequestParam("arquivo") MultipartFile file,
			@RequestParam("nome") String nome, @RequestParam("valor") String valor,
			@RequestParam("destaque") Boolean destaque, @RequestParam("descricao") String descricao) {
		if ((Objects.nonNull(file)) && (!file.isEmpty())) {
			File arquivo = new File(file.getOriginalFilename());
			try {
				FileUtils.copyInputStreamToFile(file.getInputStream(), arquivo);
				String urlArquivo = this.utilService.uploadAWS("produtos/", file.getOriginalFilename(), arquivo);
				Produto produto = montaProduto(nome, valor, destaque, descricao, arquivo, urlArquivo);

				this.produtoService.save(produto);
			} catch (IOException e) {
				System.err.println(e.getMessage());
				return ResponseEntity.badRequest().body(
						"{\"message\": \"Problemas na leitura do arquivo, tente reenvia-lo ou contate o suporte.\"}");
			} catch (IllegalArgumentException e) {
				Produto produto;
				System.err.println(e.getMessage());
				return ResponseEntity.badRequest().body(e.toString());
			} finally {
				arquivo.delete();
			}
		}
		return ResponseEntity.ok().build();
	}

	@RequestMapping(value = { "editProdutoComFoto" }, method = {
			RequestMethod.POST })
	public ResponseEntity<?> editProduto(@RequestParam("arquivo") MultipartFile file, @RequestParam("nome") String nome,
			@RequestParam("valor") String valor, @RequestParam("destaque") Boolean destaque,
			@RequestParam("descricao") String descricao, @RequestParam("id") String id) {
		Produto p = (Produto) this.produtoService.get(Long.valueOf(id));
		if ((Objects.nonNull(file)) && (!file.isEmpty())) {
			File arquivo = new File(file.getOriginalFilename());
			try {
				FileUtils.copyInputStreamToFile(file.getInputStream(), arquivo);
				String urlArquivo = this.utilService.uploadAWS("produtos/", file.getOriginalFilename(), arquivo);
				Produto produto = montaProduto(nome, valor, destaque, descricao, arquivo, urlArquivo);

				p.setNome(produto.getNome());
				p.setDescricao(produto.getDescricao());
				p.setDestaque(produto.getDestaque());
				p.setCaminhoImagem(produto.getCaminhoImagem());
				p.setValor(produto.getValor());

				this.produtoService.save(p);
			} catch (IOException e) {
				System.err.println(e.getMessage());
				return ResponseEntity.badRequest().body(
						"{\"message\": \"Problemas na leitura do arquivo, tente reenvia-lo ou contate o suporte.\"}");
			} catch (IllegalArgumentException e) {
				Produto produto;
				System.err.println(e.getMessage());
				return ResponseEntity.badRequest().body(e.toString());
			} finally {
				arquivo.delete();
			}
		}
		return ResponseEntity.ok().build();
	}

	@RequestMapping(value = { "editProduto" }, method = { RequestMethod.POST })
	public ResponseEntity<?> editProduto(@RequestParam("nome") String nome, @RequestParam("valor") String valor,
			@RequestParam("destaque") Boolean destaque, @RequestParam("descricao") String descricao,
			@RequestParam("id") String id) {
		Produto p = (Produto) this.produtoService.get(Long.valueOf(id));

		Produto produto = montaProduto(nome, valor, destaque, descricao, null, null);
		p.setNome(produto.getNome());
		p.setDescricao(produto.getDescricao());
		p.setDestaque(produto.getDestaque());
		p.setValor(produto.getValor());

		this.produtoService.save(p);

		return ResponseEntity.ok().build();
	}

	private Produto montaProduto(String nome, String valor, Boolean destaque, String descricao, File arquivo,
			String url) {
		Produto produto = new Produto();
		produto.setNome(nome);
		produto.setValor(Integer.valueOf(Integer.parseInt(valor)));
		produto.setDestaque(destaque);
		produto.setDescricao(descricao);
		produto.setCaminhoImagem(url);
		return produto;
	}

	@RequestMapping(value = { "getProduto" }, method = { RequestMethod.POST })
	public ResponseEntity<?> getProduto(@RequestBody Map<String, String> parametro) {
		Produto produto = (Produto) this.produtoService.get(Long.valueOf((String) parametro.get("id")));

		return produto == null ? ResponseEntity.notFound().build() : ResponseEntity.ok().body(produto);
	}

	@RequestMapping(value = { "getDestaques" }, method = { RequestMethod.GET })
	public ResponseEntity<?> getDestaques() {
		List<Produto> produtos = this.produtoService.findProdutoByDestaque(true);

		return produtos.isEmpty() ? ResponseEntity.notFound().build() : ResponseEntity.ok().body(produtos);
	}

	@RequestMapping(value = { "excluiProduto/{id}" }, method = {
			RequestMethod.DELETE })
	public ResponseEntity<?> excluiProduto(@PathVariable Integer id) {
		try {
			this.produtoService.delete(Long.valueOf(id.longValue()));
		} catch (Exception e) {
			ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().body(Integer.valueOf(1));
	}
}
