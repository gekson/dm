package br.com.divinomestre.ganhamais.controller;


import java.io.Serializable;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.divinomestre.ganhamais.models.AbstractModel;
import br.com.divinomestre.ganhamais.service.AbstractService;

public abstract class AbstractController<T extends AbstractModel<Long>, Long extends Serializable> {

    protected abstract AbstractService<T, Long> getService();

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> list() {
    	List<?> retorno = getService().listar();
		
		return retorno.isEmpty() ? ResponseEntity.notFound().build() : ResponseEntity.ok().body(retorno);
//        return getService().listar();
    }

    @RequestMapping(value = "/post", method = RequestMethod.POST)
    public T post(T entity) {
        return getService().save(entity);
    }

    @RequestMapping(value = "/put/{id}", method = RequestMethod.PUT)
    public HttpStatus put(@PathVariable Long id, T entity) {
        try {
            entity.setId(id);
            getService().save(entity);

            return HttpStatus.OK;
        } catch (Exception e) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public HttpStatus delete(@PathVariable Long id) {
        try {
            getService().delete(id);

            return HttpStatus.OK;
        } catch (Exception e) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }

}
