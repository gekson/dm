package br.com.divinomestre.ganhamais.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.divinomestre.ganhamais.models.Resgate;

public interface ResgateRepository extends JpaRepository<Resgate, Long> {
	Resgate findResgateByCodigo(String paramString);

	List<Resgate> findResgateByAlunoId(Long paramLong);

	List<Resgate> findResgateByAlunoIdOrderByDataSolicitacaoDesc(Long paramLong);
}
