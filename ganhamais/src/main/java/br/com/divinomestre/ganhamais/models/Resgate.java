package br.com.divinomestre.ganhamais.models;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PostPersist;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Resgate extends AbstractModel<Long> {
	@Column
	private String codigo;
	@Column
	private Date dataSolicitacao;
	@Column
	private Date dataResgate;
	@Column
	private Boolean resgateRealizado;
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "usuario_id", insertable = true, updatable = true)
	private Usuario usuarioResgate;
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "aluno_id", insertable = true, updatable = true)
	private Aluno aluno;
	@JsonManagedReference
	@ManyToMany
	@JoinTable(name = "resgate_produtos", joinColumns = { @JoinColumn(name = "resgate_id") }, inverseJoinColumns = {
			@JoinColumn(name = "produto_id") })
	private List<Produto> produtos;

	@PostPersist
	private void criaCodigo() {
		this.codigo = (this.aluno.getMatricula() + "@" + getId());
	}

	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Date getDataSolicitacao() {
		return this.dataSolicitacao;
	}

	public void setDataSolicitacao(Date dataSolicitacao) {
		this.dataSolicitacao = dataSolicitacao;
	}

	public Date getDataResgate() {
		return this.dataResgate;
	}

	public void setDataResgate(Date dataResgate) {
		this.dataResgate = dataResgate;
	}

	public Aluno getAluno() {
		return this.aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}

	public List<Produto> getProdutos() {
		return this.produtos;
	}

	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}

	public Boolean getResgateRealizado() {
		return this.resgateRealizado;
	}

	public void setResgateRealizado(Boolean resgateRealizado) {
		this.resgateRealizado = resgateRealizado;
	}

	public Usuario getUsuarioResgate() {
		return this.usuarioResgate;
	}

	public void setUsuarioResgate(Usuario usuarioResgate) {
		this.usuarioResgate = usuarioResgate;
	}
}
