package br.com.divinomestre.ganhamais.service;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.springframework.stereotype.Service;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;

@Service
public class UtilService {
	public String criptografar(String password) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		password = "Divino" + password + "Mestre";
		MessageDigest algoritmo = MessageDigest.getInstance("SHA-256");
		byte[] digestMessage = algoritmo.digest(password.getBytes("UTF-8"));
		StringBuilder hexPassword = new StringBuilder();
		for (byte aByte : digestMessage) {
			hexPassword.append(String.format("%02X", new Object[] { Integer.valueOf(0xFF & aByte) }));
		}
		return hexPassword.toString();
	}

	public String gerarSenhaAleatoria() {
		int qtdeMaximaCaracteres = 6;
		String[] caracteres = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f", "g",
				"h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B",
				"C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W",
				"X", "Y", "Z" };

		StringBuilder senha = new StringBuilder();
		for (int i = 0; i < qtdeMaximaCaracteres; i++) {
			int posicao = (int) (Math.random() * caracteres.length);
			senha.append(caracteres[posicao]);
		}
		return senha.toString();
	}

	public String uploadAWS(String dir, String name, File arquivo) {
		String bucketName = "elasticbeanstalk-us-west-2-257336811148";
		AWSCredentials credentials = new BasicAWSCredentials("AKIAJVMOKVGO2YN4DTAQ",
				"us+XqmUjSFAEUa6ellGe1bRdntBIhmP5yGdRmx3f");

		AmazonS3 s3client = new AmazonS3Client(credentials);

		String fileName = dir + name;
		s3client.putObject(
				new PutObjectRequest(bucketName, fileName, arquivo).withCannedAcl(CannedAccessControlList.PublicRead));
		return s3client.getUrl(bucketName, fileName).toString();
	}
}
