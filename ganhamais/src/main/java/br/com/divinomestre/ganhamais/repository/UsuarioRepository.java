package br.com.divinomestre.ganhamais.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.divinomestre.ganhamais.models.Usuario;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

	Usuario findUsuarioByLoginAndSenha(String usuario, String senha);

	Usuario findUsuarioByLogin(String paramString);

	List<Usuario> findUsuarioByIsAluno(Boolean paramBoolean);
}
