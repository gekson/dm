package br.com.divinomestre.ganhamais.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.divinomestre.ganhamais.models.Aluno;
import br.com.divinomestre.ganhamais.models.Categoria;
import br.com.divinomestre.ganhamais.models.Ponto;
import br.com.divinomestre.ganhamais.models.Produto;
import br.com.divinomestre.ganhamais.models.Resgate;
import br.com.divinomestre.ganhamais.models.Turma;
import br.com.divinomestre.ganhamais.models.Usuario;
import br.com.divinomestre.ganhamais.repository.AlunoRepository;
import br.com.divinomestre.ganhamais.repository.CategoriaRepository;
import br.com.divinomestre.ganhamais.repository.ProdutoRepository;
import br.com.divinomestre.ganhamais.repository.ResgateRepository;
import br.com.divinomestre.ganhamais.repository.TurmaRepository;
import br.com.divinomestre.ganhamais.repository.UsuarioRepository;

@Service
public class AlunoService extends AbstractService<Aluno, Long> {

	@Autowired
	private AlunoRepository alunoRepository;
	@Autowired
	private ProdutoRepository produtoRepository;
	@Autowired
	private CategoriaRepository categoriaRepository;
	@Autowired
	private UsuarioRepository usuarioRepository;
	@Autowired
	private TurmaRepository turmaRepository;
	@Autowired
	private UtilService utilService;
	@Autowired
	private ResgateRepository resgateRepository;

	protected JpaRepository<Aluno, Long> getRepository() {
		return this.alunoRepository;
	}

	public List<Aluno> findAlunoByMatricula(String matricula) {
		return this.alunoRepository.findByMatricula(matricula);
	}

	@Transactional
	public void addPontoAluno(Integer idAluno, List<Long> idsCategorias, Long idUsuario, Integer unidade, String mes) {
		Aluno aluno = (Aluno) this.alunoRepository.findOne(Long.valueOf(idAluno.longValue()));
		List<Categoria> categorias = this.categoriaRepository.findAll(idsCategorias);
		List<Ponto> pontos = new ArrayList();
		Usuario usuario = (Usuario) this.usuarioRepository.getOne(idUsuario);
		for (Categoria categoria : categorias) {
			Ponto ponto = new Ponto();
			ponto.setData(new Date());
			ponto.setAno(Integer.toString(LocalDate.now().getYear()));
			ponto.setCategoria(categoria);
			ponto.setQuantidade(categoria.getPontos());
			ponto.setTipo("C");
			ponto.setTurma(aluno.getTurma());
			ponto.setAluno(aluno);
			ponto.setResponsavel(usuario);
			ponto.setUnidade(unidade);
			ponto.setMes(mes);
			pontos.add(ponto);
		}
		aluno.setPontos(pontos);
		this.alunoRepository.save(aluno);
	}

	@Transactional
	public Boolean usePonto(Integer idAluno, List<Long> idProduto) {
		return resgate(idAluno, idProduto, null);
	}

	private Boolean resgate(Integer idAluno, List<Long> idProduto, String _pontos) {
		Aluno aluno = (Aluno) this.alunoRepository.findOne(Long.valueOf(idAluno.longValue()));
		List<Ponto> pontos = verificaSaldo(idProduto, _pontos, aluno);
		if (pontos == null) {
			return Boolean.valueOf(false);
		}
		aluno.setPontos(pontos);
		this.alunoRepository.save(aluno);

		return Boolean.valueOf(true);
	}

	private List<Ponto> verificaSaldo(List<Long> idProduto, String _pontos, Aluno aluno) {
		List<Produto> produtos = new ArrayList();
		if (_pontos != null) {
			Produto p = this.produtoRepository.findProdutoByNome("Lanchonete");
			p.setValor(Integer.valueOf(_pontos));
			produtos.add(p);
		} else {
			produtos = this.produtoRepository.findAll(idProduto);
		}
		String[] saldoPontos = aluno.getSaldoPontos().split(" ");
		Integer saldo = Integer.valueOf(Integer.parseInt(saldoPontos[0]));
		Integer somaProduto = Integer.valueOf(0);
		List<Ponto> pontos = new ArrayList();
		for (Produto produto : produtos) {
			if ((produto.getValor().intValue() > saldo.intValue()) || (somaProduto.intValue() > saldo.intValue())) {
				return null;
			}
			somaProduto = Integer.valueOf(somaProduto.intValue() + produto.getValor().intValue());
			if (somaProduto.intValue() > saldo.intValue()) {
				return null;
			}
			Ponto ponto = new Ponto();
			ponto.setData(new Date());
			ponto.setAno(Integer.toString(LocalDate.now().getYear()));
			ponto.setQuantidade(produto.getValor());
			if (_pontos != null) {
				ponto.setQuantidade(Integer.valueOf(_pontos));
			}
			ponto.setProduto(produto);
			ponto.setTipo("D");
			ponto.setAluno(aluno);
			pontos.add(ponto);
		}
		return pontos;
	}

	public Boolean resgateLanchonete(Long idAluno, String pontos) {
		return resgate(Integer.valueOf(idAluno.intValue()), new ArrayList(), pontos);
	}

	@Transactional
	public Boolean cargaArquivo(FileInputStream arquivo) {
		try {
			HSSFWorkbook workbook = new HSSFWorkbook(arquivo);

			HSSFSheet plan1 = workbook.getSheetAt(0);

			Iterator<Row> rowIterator = plan1.iterator();
			int linha = -1;
			List<Aluno> alunos = new ArrayList();

			leArquivo(rowIterator, linha, alunos);

			workbook.close();
			arquivo.close();

			return salvaUsuarios(alunos);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return Boolean.valueOf(false);
	}

	@Transactional
	public Boolean cargaArquivoXlsx(FileInputStream arquivo, File file) {
		try {
			XSSFWorkbook workbook = new XSSFWorkbook(arquivo);

			XSSFSheet plan1 = workbook.getSheetAt(0);

			Iterator<Row> rowIterator = plan1.iterator();
			int linha = -1;
			List<Aluno> alunos = new ArrayList();

			leArquivoXlsx(rowIterator, linha, alunos);

			arquivo.close();
			FileOutputStream outFile = new FileOutputStream(file);
			workbook.write(outFile);
			outFile.close();
			workbook.close();

			return salvaUsuarios(alunos);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return Boolean.valueOf(false);
	}

	private Boolean salvaUsuarios(List<Aluno> alunos) {
		int l = 0;
		for (Aluno aluno : alunos) {
			List<Aluno> validaAluno = this.alunoRepository.findByMatricula(aluno.getMatricula());
			if (validaAluno.isEmpty()) {
				List<Turma> validaTurma = this.turmaRepository.findTurmaByCodigo(aluno.getTurma().getCodigo());
				if (!validaTurma.isEmpty()) {
					aluno.setTurma((Turma) validaTurma.get(0));
				}
				Usuario user = new Usuario();
				user.setLogin(aluno.getMatricula());
				try {
					user.setSenha(this.utilService.criptografar(aluno.getSenha()));
				} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				user.setIsAluno(Boolean.valueOf(true));
				user.setAluno(aluno);
				this.usuarioRepository.save(user);

				System.out.println(l + " - " + aluno.getTurma().getCodigo());
				System.out.println(aluno.getMatricula() + " = " + user.getAluno().getNome() + " = " + user.getSenha());
				l++;
			}
		}
		return Boolean.valueOf(true);
	}

	private void leArquivo(Iterator<Row> rowIterator, int linha, List<Aluno> alunos) {
		Turma turmaAux = new Turma();
		while (rowIterator.hasNext()) {
			Row row = (Row) rowIterator.next();
			Iterator<Cell> cellIterator = row.cellIterator();

			Aluno aluno = new Aluno();
			while (cellIterator.hasNext()) {
				Cell cell = (Cell) cellIterator.next();
				if ((cell.getStringCellValue().contains("EDUCAÇÃO INFANTIL"))
						|| (cell.getStringCellValue().contains("ENSINO FUNDAMENTAL"))
						|| (cell.getStringCellValue().contains("ENSINO MÉDIO"))) {
					String[] splitTurma = cell.getStringCellValue().split("/");
					turmaAux.setAno(splitTurma[2].trim());
					turmaAux.setNome(splitTurma[0] + splitTurma[1].trim());
					turmaAux.setCodigo(splitTurma[3].trim());

					linha = row.getRowNum() + 3;
				}
				if ((cell.getColumnIndex() == 1) && (cell.getStringCellValue().contains("Total"))) {
					linha = 0;
				}
				if (linha == row.getRowNum()) {
					if (cell.getColumnIndex() == 3) {
						aluno.setMatricula(cell.getStringCellValue());
						Turma turma = new Turma();
						turma.setAno(turmaAux.getAno());
						turma.setNome(turmaAux.getNome());
						turma.setCodigo(turmaAux.getCodigo());
						aluno.setTurma(turma);
					}
					if (cell.getColumnIndex() == 4) {
						aluno.setNome(cell.getStringCellValue());
						alunos.add(aluno);

						linha = row.getRowNum() + 2;
					}
				}
			}
		}
	}

	private void leArquivoXlsx(Iterator<Row> rowIterator, int linha, List<Aluno> alunos) {
		Turma turmaAux = new Turma();
		while (rowIterator.hasNext()) {
			Row row = (Row) rowIterator.next();
			if (row.getRowNum() == 0) {
				Cell cellCabecalhoSenha = row.createCell(11);
				cellCabecalhoSenha.setCellValue("SENHA APP");
			} else {
				Aluno aluno = new Aluno();
				if ((!row.getCell(6).getStringCellValue().isEmpty()) && (row.getCell(3).getCellType() != 3)) {
					turmaAux.setAno(String.valueOf(LocalDate.now().getYear()));
					turmaAux.setNome(row.getCell(6).getStringCellValue());
					turmaAux.setCodigo(row.getCell(7).getStringCellValue());

					aluno.setMatricula(
							Integer.toString(Double.valueOf(row.getCell(3).getNumericCellValue()).intValue()));
					Turma turma = new Turma();
					turma.setAno(turmaAux.getAno());
					turma.setNome(turmaAux.getNome());
					turma.setCodigo(turmaAux.getCodigo());
					aluno.setTurma(turma);
					aluno.setNome(row.getCell(0).getStringCellValue());
					aluno.setSenha(this.utilService.gerarSenhaAleatoria());
					aluno.setEmail(row.getCell(4).getStringCellValue());
					alunos.add(aluno);

					Cell cellSenha = row.createCell(11);
					cellSenha.setCellValue(aluno.getSenha());
				}
			}
		}
	}

	public Resgate pedidoResgate(Integer idAluno, List<Long> idsProdutos) {
		Aluno aluno = (Aluno) this.alunoRepository.findOne(Long.valueOf(idAluno.longValue()));
		List<Ponto> pontos = verificaSaldo(idsProdutos, null, aluno);
		if (pontos == null) {
			return null;
		}
		List<Produto> produtos = this.produtoRepository.findAll(idsProdutos);
		Resgate resgate = new Resgate();
		resgate.setAluno(aluno);
		resgate.setDataSolicitacao(new Date());
		resgate.setProdutos(produtos);
		resgate.setResgateRealizado(Boolean.valueOf(false));

		this.resgateRepository.save(resgate);

		return resgate;
	}
}
