package br.com.divinomestre.ganhamais.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Aluno extends AbstractModel<Long> {

	private static final long serialVersionUID = 8934147526845057747L;
	
	@Column
	private String nome;
	
	@Column
	private String matricula;
	
	@JsonManagedReference
	@ManyToOne
	@JoinColumn(name="turma_id")
	private Turma turma;
	
	@JsonManagedReference
	@OneToMany(mappedBy="aluno", cascade=CascadeType.ALL)
	private List<Ponto> pontos;
	
	@Column
	private String email;
	
	@Column
	private String foto;
	
	@Transient
	private String senha;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}	

	public Turma getTurma() {
		return turma;
	}

	public void setTurma(Turma turma) {
		this.turma = turma;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Ponto> getPontos() {
		return pontos;
	}

	public void setPontos(List<Ponto> pontos) {
		this.pontos = pontos;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}
	public String getSenha()
	{
	    return this.senha;
	}
	  
	public void setSenha(String senha)
	{
	    this.senha = senha;
	}
	  
	public String getSaldoPontos() {
		Integer pontosCredito = 0;
		Integer pontosDebito = 0;
		for (Ponto ponto : pontos) {
			if(ponto.getTipo().equals("C")){
				pontosCredito += ponto.getQuantidade();
			}else{
				pontosDebito += ponto.getQuantidade();
			}
		}
		Integer saldo = pontosCredito - pontosDebito;
		return saldo.toString()+" Pts";
	}
		
}
