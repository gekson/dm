package br.com.divinomestre.ganhamais.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import br.com.divinomestre.ganhamais.models.Categoria;
import br.com.divinomestre.ganhamais.repository.CategoriaRepository;

@Service
public class CategoriaService extends AbstractService<Categoria, Long> {

	@Autowired
	CategoriaRepository categoriaRepository;

	@Override
	protected JpaRepository<Categoria, Long> getRepository() {
		return categoriaRepository;
	}

	public void salvarCategoria(String nome, Integer pontos, Boolean coletivo, Boolean ativo, String unidadeMedida, Boolean porUnidade, Boolean pontoVariavel) {
		Categoria categoria = new Categoria();
		categoria.setNome(nome);
		categoria.setPontos(pontos);
		categoria.setColetivo(coletivo);
		categoria.setAtivo(ativo);
		categoria.setUnidadeMedida(unidadeMedida);
		categoria.setPorUnidade(porUnidade);
		categoria.setPontoVariavel(pontoVariavel);

		this.categoriaRepository.save(categoria);
	}
}
