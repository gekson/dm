package br.com.divinomestre.ganhamais.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import br.com.divinomestre.ganhamais.models.Produto;
import br.com.divinomestre.ganhamais.repository.ProdutoRepository;

@Service
public class ProdutoService extends AbstractService<Produto, Long> {

	@Autowired
	private ProdutoRepository produtoRepository;

	@Override
	protected JpaRepository<Produto, Long> getRepository() {

		return produtoRepository;
	}

	public List<Produto> findProdutoByDestaque(boolean b) {
		return this.produtoRepository.findProdutoByDestaque(true);
	}

}
