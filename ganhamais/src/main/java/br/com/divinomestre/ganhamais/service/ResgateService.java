package br.com.divinomestre.ganhamais.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import br.com.divinomestre.ganhamais.models.Resgate;
import br.com.divinomestre.ganhamais.models.Usuario;
import br.com.divinomestre.ganhamais.repository.ResgateRepository;
import br.com.divinomestre.ganhamais.repository.UsuarioRepository;

@Service
public class ResgateService extends AbstractService<Resgate, Long> {
	@Autowired
	private ResgateRepository resgateRepository;
	@Autowired
	private UsuarioRepository usuarioRepository;

	protected JpaRepository<Resgate, Long> getRepository() {
		return this.resgateRepository;
	}

	public Resgate findResgateByCodigo(String codigo) {
		return this.resgateRepository.findResgateByCodigo(codigo);
	}

	public void setResgateRealizado(Long id, Long idUsuario) {
		Resgate resgate = (Resgate) this.resgateRepository.getOne(id);
		Usuario usuario = (Usuario) this.usuarioRepository.getOne(idUsuario);

		resgate.setUsuarioResgate(usuario);
		resgate.setDataResgate(new Date());
		resgate.setResgateRealizado(Boolean.valueOf(true));

		this.resgateRepository.save(resgate);
	}

	public Resgate findResgateByAluno(Long id) {
		List<Resgate> r = this.resgateRepository.findResgateByAlunoIdOrderByDataSolicitacaoDesc(id);
		return (Resgate) r.get(0);
	}
}
