package br.com.divinomestre.ganhamais.controller;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.divinomestre.ganhamais.models.Categoria;
import br.com.divinomestre.ganhamais.models.Resgate;
import br.com.divinomestre.ganhamais.models.Usuario;
import br.com.divinomestre.ganhamais.service.AlunoService;
import br.com.divinomestre.ganhamais.service.ResgateService;
import br.com.divinomestre.ganhamais.service.TurmaService;
import br.com.divinomestre.ganhamais.service.UsuarioService;

@RestController
@CrossOrigin(origins = { "*" })
@RequestMapping({ "/ponto" })
public class PontoController {
	@Autowired
	private TurmaService turmaService;
	@Autowired
	private AlunoService alunoService;
	@Autowired
	private UsuarioService usuarioService;
	@Autowired
	private ResgateService resgateService;

	@RequestMapping(value = { "addPontoTurma" }, method = {RequestMethod.POST })
	public ResponseEntity<?> addPontoTurma(@RequestBody Map<String, List> parametros) {
		List<Long> ids = new ArrayList();
		for (Integer inteiro : (List<Integer>)parametros.get("ids")) {
			ids.add(Long.valueOf(inteiro.longValue()));
		}
		Object turmas = this.turmaService.findByIds(ids);

		List<Long> idsCategorias = new ArrayList();
		for (Integer inteiro : (List<Integer>)parametros.get("categorias")) {
			idsCategorias.add(Long.valueOf(inteiro.longValue()));
		}
		Integer idUsuario = (Integer) ((List) parametros.get("idUsuario")).get(0);
		String unidade = (String) ((List) parametros.get("unidade")).get(0);
		String mes = (String) ((List) parametros.get("mes")).get(0);
		unidade = "0";

		this.turmaService.addPontoTurma((List) turmas, idsCategorias, Long.valueOf(idUsuario.longValue()),
				Integer.valueOf(unidade), mes);

		return ((List) turmas).isEmpty() ? ResponseEntity.notFound().build() : ResponseEntity.ok().body(turmas);
	}

	@RequestMapping(value = { "addPontoAluno" }, method = {RequestMethod.POST })
	public ResponseEntity<?> addPontoAluno(@RequestBody Map<String, List> parametros) {
		List<Long> idsCategorias = new ArrayList();
		for (Integer inteiro : (List<Integer>)parametros.get("categorias")) {
			idsCategorias.add(Long.valueOf(inteiro.longValue()));
		}
		Integer idAluno = (Integer) ((List) parametros.get("id")).get(0);
		Integer idUsuario = (Integer) ((List) parametros.get("idUsuario")).get(0);
		String unidade = (String) ((List) parametros.get("unidade")).get(0);
		String mes = (String) ((List) parametros.get("mes")).get(0);
		unidade = "0";
		try {
			this.alunoService.addPontoAluno(idAluno, idsCategorias, Long.valueOf(idUsuario.longValue()),
					Integer.valueOf(unidade), mes);
			return ResponseEntity.ok().body(idAluno);
		} catch (Exception e) {
		}
		return ResponseEntity.notFound().build();
	}

	private ResponseEntity<?> resgatePonto(Map<String, List> parametros) {
		List<Long> idsProdutos = new ArrayList();

		Integer idAluno = (Integer) ((List) parametros.get("id")).get(0);
		for (Integer inteiro : (List<Integer>) parametros.get("idProduto")) {
			idsProdutos.add(Long.valueOf(inteiro.longValue()));
		}
		try {
			Boolean temSaldo = this.alunoService.usePonto(idAluno, idsProdutos);
			if (temSaldo.booleanValue()) {
				return ResponseEntity.ok().body(idAluno);
			}
			return ResponseEntity.ok().body(Integer.valueOf(0));
		} catch (Exception e) {
		}
		return ResponseEntity.notFound().build();
	}

	@CrossOrigin(origins = { "*" }, maxAge = 3600L)
	@RequestMapping(value = { "usePonto" }, method = {RequestMethod.POST })
	public ResponseEntity<?> usePonto(@RequestBody Map<String, List> parametros)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String usuario = (String) ((List) parametros.get("matricula")).get(0);
		String senha = (String) ((List) parametros.get("senha")).get(0);

		Usuario usuarioLogado = this.usuarioService.findUsuarioByLoginAndSenha(usuario, senha);
		if (usuarioLogado == null) {
			return ResponseEntity.notFound().build();
		}
		return resgate(parametros, Boolean.valueOf(false));
	}

	private ResponseEntity<?> resgate(Map<String, List> parametros, Boolean isPedido) {
		List<Long> idsProdutos = new ArrayList();

		Integer idAluno = (Integer) ((List) parametros.get("id")).get(0);
		for (Integer inteiro : (List<Integer>) parametros.get("idProduto")) {
			idsProdutos.add(Long.valueOf(inteiro.longValue()));
		}
		Boolean temSaldo = Boolean.valueOf(false);
		Resgate resgate = new Resgate();
		resgate = null;
		try {
			if (isPedido.booleanValue()) {
				resgate = this.alunoService.pedidoResgate(idAluno, idsProdutos);
				if (resgate != null) {
					temSaldo = Boolean.valueOf(true);
				}
			} else {
				temSaldo = this.alunoService.usePonto(idAluno, idsProdutos);
			}
			if (temSaldo.booleanValue()) {
				if (resgate != null) {
					return ResponseEntity.ok().body(resgate);
				}
				return ResponseEntity.ok().body(idAluno);
			}
			return ResponseEntity.ok().body(Integer.valueOf(0));
		} catch (Exception e) {
		}
		return ResponseEntity.notFound().build();
	}

	@RequestMapping(value = { "getCategoriasColetivo" }, method = {RequestMethod.GET })
	public ResponseEntity<?> getCategoriasColetivo() {
		List<Categoria> retorno = this.turmaService.getCategoriasColetivo();

		return retorno.isEmpty() ? ResponseEntity.notFound().build() : ResponseEntity.ok().body(retorno);
	}

	@RequestMapping(value = { "getCategoriasIndividual" }, method = {RequestMethod.GET })
	public ResponseEntity<?> getCategoriasIndividual() {
		List<Categoria> retorno = this.turmaService.getCategoriasIndividual();

		return retorno.isEmpty() ? ResponseEntity.notFound().build() : ResponseEntity.ok().body(retorno);
	}

	@RequestMapping(value = { "resgateLanchonete" }, method = {RequestMethod.POST })
	public ResponseEntity<?> resgateLanchonete(@RequestBody Map<String, String> parametros)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String pontos = (String) parametros.get("pontos");
		String usuario = (String) parametros.get("matricula");
		String senha = (String) parametros.get("senha");

		Usuario usuarioLogado = this.usuarioService.findUsuarioByLoginAndSenha(usuario, senha);
		if (usuarioLogado == null) {
			return ResponseEntity.notFound().build();
		}
		try {
			Boolean temSaldo = this.alunoService.resgateLanchonete((Long) usuarioLogado.getAluno().getId(), pontos);
			if (temSaldo.booleanValue()) {
				return ResponseEntity.ok().body(Integer.valueOf(1));
			}
			return ResponseEntity.ok().body(Integer.valueOf(0));
		} catch (Exception e) {
		}
		return ResponseEntity.notFound().build();
	}

	@CrossOrigin(origins = { "*" }, maxAge = 3600L)
	@RequestMapping(value = { "pedidoResgate" }, method = {RequestMethod.POST })
	public ResponseEntity<?> pedidoResgate(@RequestBody Map<String, List> parametros) {
		return resgate(parametros, Boolean.valueOf(true));
	}

	@RequestMapping(value = { "findResgateByCodigo" }, method = {RequestMethod.POST })
	public ResponseEntity<?> findResgateByCodigo(@RequestBody Map<String, String> parametros) {
		String codigo = (String) parametros.get("codigo");

		Resgate resgate = this.resgateService.findResgateByCodigo(codigo);
		return resgate == null ? ResponseEntity.notFound().build() : ResponseEntity.ok().body(resgate);
	}

	@RequestMapping(value = { "resgateRealizado" }, method = {RequestMethod.POST })
	public ResponseEntity<?> setResgateRealizado(@RequestBody Map<String, String> parametros) {
		Long id = Long.valueOf((String) parametros.get("id"));
		Long usuario = Long.valueOf((String) parametros.get("usuario"));
		try {
			this.resgateService.setResgateRealizado(Long.valueOf(id.longValue()), Long.valueOf(usuario.longValue()));

			return ResponseEntity.ok().body(Integer.valueOf(1));
		} catch (Exception e) {
		}
		return ResponseEntity.notFound().build();
	}

	@RequestMapping(value = { "getResgateAluno/{id}" }, method = {RequestMethod.GET })
	public ResponseEntity<?> findResgateByAluno(@PathVariable("id") Integer idAluno) {
		Long id = Long.valueOf(idAluno.intValue());

		Resgate resgate = this.resgateService.findResgateByAluno(id);
		return resgate == null ? ResponseEntity.notFound().build() : ResponseEntity.ok().body(resgate);
	}
}
