package br.com.divinomestre.ganhamais.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.divinomestre.ganhamais.models.Produto;

@Repository
public interface ProdutoRepository extends JpaRepository<Produto, Long> {

	Produto findProdutoByNome(String paramString);

	List<Produto> findProdutoByDestaque(boolean paramBoolean);
}
