package br.com.divinomestre.ganhamais.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.divinomestre.ganhamais.models.Turma;
import br.com.divinomestre.ganhamais.service.AbstractService;
import br.com.divinomestre.ganhamais.service.TurmaService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/turma")
public class TurmaController extends AbstractController<Turma, Long> {

	@Autowired
	private TurmaService turmaService;
	
	@Override
	protected AbstractService<Turma, Long> getService() {		
		return turmaService;
	}

}
