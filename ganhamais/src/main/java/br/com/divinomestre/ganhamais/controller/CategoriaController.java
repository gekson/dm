package br.com.divinomestre.ganhamais.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.divinomestre.ganhamais.models.Categoria;
import br.com.divinomestre.ganhamais.service.AbstractService;
import br.com.divinomestre.ganhamais.service.CategoriaService;

@RestController
@CrossOrigin(origins = { "*" })
@RequestMapping({ "/categoria" })
public class CategoriaController extends AbstractController<Categoria, Long> {
	@Autowired
	CategoriaService categoriaService;

	protected AbstractService<Categoria, Long> getService() {
		return this.categoriaService;
	}

	@RequestMapping(value = { "salvarCategoria" }, method = {
			org.springframework.web.bind.annotation.RequestMethod.POST })
	public ResponseEntity<?> salvarCategoria(@RequestBody Map<String, Object> parametros) {
		String nome = (String) parametros.get("nome");
		Integer pontos = (Integer) parametros.get("pontos");
		Boolean coletivo = (Boolean) parametros.get("coletivo");
		Boolean ativo = (Boolean) parametros.get("ativo");
		String unidadeMedida = (String) parametros.get("unidadeMedida");
		Boolean porUnidade = (Boolean) parametros.get("porUnidade");
		Boolean pontoVariavel = (Boolean) parametros.get("pontoVariavel");
		try {
			this.categoriaService.salvarCategoria(nome, pontos, coletivo, ativo, unidadeMedida, porUnidade, pontoVariavel);
			return ResponseEntity.ok().body(Integer.valueOf(1));
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(e.toString());
		}
	}
}
