package br.com.divinomestre.ganhamais.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Ponto extends AbstractModel<Long> {

	@Column
	private String ano;

	@Column
	private Integer quantidade;

	/**
	 * C -> Crédito D -> Débito
	 */
	@Column
	private String tipo;

	@Column
	private Date data;

	@Column
	private Integer unidade;

	@Column
	private String mes;
	
	@Column
	private String observacao;

	@ManyToOne
	@JoinColumn(name = "categoria_id", insertable = true, updatable = true)
	private Categoria categoria;

	@JsonManagedReference
	@ManyToOne
	@JoinColumn(name = "turma_id", insertable = true, updatable = true)
	private Turma turma;

	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "aluno_id", insertable = true, updatable = true)
	private Aluno aluno;

	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "produto_id", insertable = true, updatable = true)
	private Produto produto;

	@ManyToOne
	@JoinColumn(name = "usuario_id", insertable = true, updatable = true)
	private Usuario responsavel;

	public String getAno() {
		return ano;
	}

	public void setAno(String ano) {
		this.ano = ano;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public Turma getTurma() {
		return turma;
	}

	public void setTurma(Turma turma) {
		this.turma = turma;
	}

	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Usuario getResponsavel() {
		return this.responsavel;
	}

	public void setResponsavel(Usuario responsavel) {
		this.responsavel = responsavel;
	}

	public Integer getUnidade() {
		return this.unidade;
	}

	public void setUnidade(Integer unidade) {
		this.unidade = unidade;
	}

	public String getMes() {
		return this.mes;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
}
