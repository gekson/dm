package br.com.divinomestre.ganhamais.models;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Categoria extends AbstractModel<Long> {

	@Column
	private String nome;
	
	@Column
	private Integer pontos;
	
	@Column
	private String unidadeMedida;
	
	@Column
	private Boolean coletivo; 
	
	@Column
	private Boolean ativo;
	
	@Column
	private Boolean porUnidade;
	
	@Column
	private Boolean pontoVariavel;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getPontos() {
		return pontos;
	}

	public void setPontos(Integer pontos) {
		this.pontos = pontos;
	}

	public String getUnidadeMedida() {
		return unidadeMedida;
	}

	public void setUnidadeMedida(String unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}

	public Boolean getColetivo() {
		return coletivo;
	}

	public void setColetivo(Boolean coletivo) {
		this.coletivo = coletivo;
	}
	public Boolean getAtivo()
	{
	    return this.ativo;
	}
	  
	public void setAtivo(Boolean ativo)
	{
	    this.ativo = ativo;
	}

	public Boolean getPorUnidade() {
		return porUnidade;
	}

	public void setPorUnidade(Boolean porUnidade) {
		this.porUnidade = porUnidade;
	}

	public Boolean getPontoVariavel() {
		return pontoVariavel;
	}

	public void setPontoVariavel(Boolean pontoVariavel) {
		this.pontoVariavel = pontoVariavel;
	}
	
}
