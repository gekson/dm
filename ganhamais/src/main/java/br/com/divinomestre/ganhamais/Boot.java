package br.com.divinomestre.ganhamais;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.divinomestre.ganhamais.models.Aluno;
import br.com.divinomestre.ganhamais.models.Categoria;
import br.com.divinomestre.ganhamais.models.Turma;
import br.com.divinomestre.ganhamais.models.Usuario;
import br.com.divinomestre.ganhamais.service.AlunoService;
import br.com.divinomestre.ganhamais.service.CategoriaService;
import br.com.divinomestre.ganhamais.service.TurmaService;
import br.com.divinomestre.ganhamais.service.UsuarioService;

import org.springframework.stereotype.Controller;

@SpringBootApplication
@Controller
public class Boot
{
	@Autowired
	private TurmaService turmaService;
	
	@Autowired
	private AlunoService alunoService;
	
	@Autowired
	private CategoriaService categoriaService;
	
	@Autowired
	private UsuarioService usuarioService;

   public static void main(String[] args)
   {
      SpringApplication.run(Boot.class, args);
   }

//   @RequestMapping("/")
//   @ResponseBody
   public String home()
   {
      return "home";
   }
   
   @RequestMapping("/carga")
   @ResponseBody
   public String cargaInicial(){
	   try {
		   Turma t = new Turma();
		   t.setAno("2017");
		   t.setNome("Teste_4 Ano A");
		   turmaService.save(t);
		   
		   Aluno a = new Aluno();
		   a.setNome("Aluno Teste 1");
		   a.setEmail("aluno@aluno.com");
		   a.setFoto("/assets/img/faces/marc.jpg");
		   a.setMatricula("A1");
		   a.setTurma(t);		   
		   alunoService.save(a);
		   
		   Usuario u = new Usuario();
		   u.setLogin("admin");
		   u.setSenha("123");
		   u.setIsAluno(false);			
		   usuarioService.save(u);
		   
		   Usuario uA = new Usuario();
		   uA.setLogin(a.getMatricula());
		   uA.setSenha(a.getMatricula());
		   uA.setIsAluno(true);			
		   usuarioService.save(uA);
		   
		   Categoria c1 = new Categoria();
		   c1.setNome("Aproveitamento Coletivo");
		   c1.setPontos(5);
		   c1.setColetivo(true);
		   categoriaService.save(c1);
		   
		   Categoria c2 = new Categoria();
		   c2.setNome("Disciplina Coletiva");
		   c2.setPontos(5);
		   c2.setColetivo(true);
		   categoriaService.save(c2);
		   
		   Categoria c3 = new Categoria();
		   c3.setNome("Disciplina Coletiva Organizacional");
		   c3.setPontos(5);
		   c3.setColetivo(true);
		   categoriaService.save(c3);
		   
		   Categoria c4 = new Categoria();
		   c4.setNome("Aproveitamento Individual");
		   c4.setPontos(30);
		   c4.setColetivo(false);
		   categoriaService.save(c4);
		   
		   Categoria c5 = new Categoria();
		   c5.setNome("Disciplina Individual");
		   c5.setPontos(5);
		   c5.setColetivo(false);
		   categoriaService.save(c5);
		   
		   Categoria c6 = new Categoria();
		   c6.setNome("Ação Colaborativa");
		   c6.setPontos(10);
		   c6.setColetivo(false);
		   categoriaService.save(c6);
		   
		   return "Carga Completa";
		} catch (Exception e) {
			return "Erro na carga: " +  e.getMessage();
		}	   
   }
   
   @RequestMapping("/useraluno")
   @ResponseBody
   public String usuarioInicial(){
	   try {		   
		   Aluno a = alunoService.get(1l);
		   
//		   Usuario uA = new Usuario();
//		   uA.setLogin(a.getMatricula());
//		   uA.setSenha(a.getMatricula());
//		   uA.setIsAluno(true);		
//		   uA.setAluno(a);
//		   usuarioService.save(uA);
		   
		   Usuario u = usuarioService.get(2l);
		   u.setAluno(a);		
		   usuarioService.save(u);
		   
		   return "Aluno associado";
		} catch (Exception e) {
			return "Erro na carga: " +  e.getMessage();
		}	   
   }
}
