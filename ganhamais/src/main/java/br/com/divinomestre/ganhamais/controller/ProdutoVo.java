package br.com.divinomestre.ganhamais.controller;

import org.springframework.web.multipart.MultipartFile;

public class ProdutoVo {

	private MultipartFile file;
	private Object produto;
	public MultipartFile getFile() {
		return file;
	}
	public void setFile(MultipartFile file) {
		this.file = file;
	}
	public Object getProduto() {
		return produto;
	}
	public void setProduto(Object produto) {
		this.produto = produto;
	}
	
}
