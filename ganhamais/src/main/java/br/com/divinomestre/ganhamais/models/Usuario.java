package br.com.divinomestre.ganhamais.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Usuario extends AbstractModel<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7744589402777442743L;

	@Column
	private String login;
	
	@Column
	private String senha;
	
	@JsonManagedReference
	@OneToOne(cascade = CascadeType.ALL, optional = true, fetch = FetchType.EAGER, orphanRemoval = false)
	@JoinColumn(name="aluno_id")
	private Aluno aluno;
	
	@Column
	private Boolean isAluno;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}

	public Boolean getIsAluno() {
		return isAluno;
	}

	public void setIsAluno(Boolean isAluno) {
		this.isAluno = isAluno;
	}
		
}
