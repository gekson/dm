package br.com.divinomestre.ganhamais.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.divinomestre.ganhamais.models.Turma;

@Repository
public interface TurmaRepository extends JpaRepository<Turma, Long> {
	List<Turma> findTurmaByCodigo(String paramString);
}
