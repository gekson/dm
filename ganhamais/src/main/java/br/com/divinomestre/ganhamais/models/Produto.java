package br.com.divinomestre.ganhamais.models;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Produto extends AbstractModel<Long> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4608834013371254942L;

	@Column
	private String nome;
	
	@Column
	private String descricao;
	
	@Column
	private Integer valor;
	
	@Column
	private Boolean destaque;
	
	@Column
	private String caminhoImagem;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getValor() {
		return valor;
	}

	public void setValor(Integer valor) {
		this.valor = valor;
	}

	public Boolean getDestaque() {
		return destaque;
	}

	public void setDestaque(Boolean destaque) {
		this.destaque = destaque;
	}

	public String getCaminhoImagem() {
		return caminhoImagem;
	}

	public void setCaminhoImagem(String caminhoImagem) {
		this.caminhoImagem = caminhoImagem;
	}
	
	public String getDestaqueSim(){
		return this.destaque?"<i class=material-icons''>edit</i>":"";
	}
	
}
