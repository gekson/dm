package br.com.divinomestre.ganhamais.controller;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.divinomestre.ganhamais.models.Usuario;
import br.com.divinomestre.ganhamais.service.AbstractService;
import br.com.divinomestre.ganhamais.service.UsuarioService;
import br.com.divinomestre.ganhamais.service.UtilService;

@RestController
@CrossOrigin(origins = { "*" })
@RequestMapping({ "/usuario" })
public class UsuarioController extends AbstractController<Usuario, Long> {
	@Autowired
	private UsuarioService usuarioService;
	@Autowired
	private UtilService utilService;

	protected AbstractService<Usuario, Long> getService() {
		return this.usuarioService;
	}

	@RequestMapping(value = { "findUsuarioByLoginAndSenha" }, method = {
			RequestMethod.POST })
	public ResponseEntity<?> login(@RequestBody Map<String, String> parametros)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String usuario = (String) parametros.get("matricula");
		String senha = (String) parametros.get("senha");

		Usuario usuarioLogado = this.usuarioService.findUsuarioByLoginAndSenha(usuario, senha);
		return usuarioLogado == null ? ResponseEntity.notFound().build() : ResponseEntity.ok().body(usuarioLogado);
	}

	@RequestMapping(value = { "findUsuarioByLogin" }, method = {
			RequestMethod.POST })
	public ResponseEntity<?> findUsuarioByLogin(@RequestBody Map<String, String> parametros) {
		String login = (String) parametros.get("matricula");

		Usuario usuario = this.usuarioService.findUsuarioByLogin(login);
		return usuario == null ? ResponseEntity.notFound().build() : ResponseEntity.ok().body(usuario);
	}

	@RequestMapping(value = { "alteraSenha" }, method = { RequestMethod.POST })
	public ResponseEntity<?> alteraSenha(@RequestBody Map<String, String> parametros) {
		String id = (String) parametros.get("id");
		String senha = (String) parametros.get("senha");

		Usuario u = (Usuario) this.usuarioService.get(Long.valueOf(id));
		try {
			u.setSenha(this.utilService.criptografar(senha));
			this.usuarioService.save(u);
			return ResponseEntity.ok().body(u);
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(e.toString());
		}
	}

	@RequestMapping(value = { "criaUsuario" }, method = { RequestMethod.POST })
	public ResponseEntity<?> criaUsuario(@RequestBody Map<String, String> parametros) {
		String login = (String) parametros.get("login");
		String senha = (String) parametros.get("senha");
		try {
			this.usuarioService.criaUsuario(login, senha);
			return ResponseEntity.ok().body(Integer.valueOf(1));
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(e.toString());
		}
	}

	public ResponseEntity<?> getUsuarios() {
		List<Usuario> usuarios = this.usuarioService.findUsuarioByIsAluno(Boolean.valueOf(false));

		return usuarios.isEmpty() ? ResponseEntity.notFound().build() : ResponseEntity.ok().body(usuarios);
	}
}