package br.com.divinomestre.ganhamais.daos;

import java.util.List;

import br.com.divinomestre.ganhamais.models.Aluno;

public interface AlunoDao {

	List<Aluno> findAlunos();
}
