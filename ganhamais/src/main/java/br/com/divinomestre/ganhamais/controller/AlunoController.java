package br.com.divinomestre.ganhamais.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.com.divinomestre.ganhamais.models.Aluno;
import br.com.divinomestre.ganhamais.service.AbstractService;
import br.com.divinomestre.ganhamais.service.AlunoService;
import br.com.divinomestre.ganhamais.service.UtilService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/aluno")
public class AlunoController extends AbstractController<Aluno, Long> {

	@Autowired
	private AlunoService alunoService;
	@Autowired
	private UtilService utilService;

	@Override
	protected AbstractService<Aluno, Long> getService() {
		return alunoService;
	}

	@RequestMapping(value = "findAlunoByMatricula", method = RequestMethod.POST)
	public ResponseEntity<?> findAlunoByMatricula(@RequestBody Map<String, String> parametros) {

		List<Aluno> alunos = alunoService.findAlunoByMatricula(parametros.get("matricula"));

		return alunos.isEmpty() ? ResponseEntity.notFound().build() : ResponseEntity.ok().body(alunos);
	}

	@RequestMapping(value = "cargaArquivo", method = RequestMethod.POST)
	public ResponseEntity<?> cargaArquivo(@RequestParam("arquivo") MultipartFile file) {
		if ((Objects.nonNull(file)) && (!file.isEmpty())) {
			File arquivo = new File(file.getOriginalFilename());
			try {
				FileUtils.copyInputStreamToFile(file.getInputStream(), arquivo);
				FileInputStream arquivoStream = new FileInputStream(arquivo);
				this.alunoService.cargaArquivoXlsx(arquivoStream, arquivo);
				this.utilService.uploadAWS("cargaAlunos/", file.getOriginalFilename(), arquivo);
			} catch (IOException e) {
				System.err.println(e.getMessage());
				return ResponseEntity.badRequest().body(
						"{\"message\": \"Problemas na leitura do arquivo, tente reenvia-lo ou contate o suporte.\"}");
			} catch (IllegalArgumentException e) {
				ResponseEntity localResponseEntity;
				System.err.println(e.getMessage());
				return ResponseEntity.badRequest().body(e.toString());
			} finally {
				arquivo.delete();
			}
		}
		return ResponseEntity.ok().build();
	}
}
