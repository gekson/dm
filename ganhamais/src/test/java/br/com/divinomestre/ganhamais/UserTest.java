package br.com.divinomestre.ganhamais;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import javax.validation.constraints.AssertTrue;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import br.com.divinomestre.ganhamais.models.Usuario;
import br.com.divinomestre.ganhamais.service.UsuarioService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Boot.class)
@WebAppConfiguration
public class UserTest {

	@Autowired
	private UsuarioService usuarioService;
	
//	@Test
	public void testUser() {
		Usuario u = new Usuario();
		u.setLogin("admin");
		u.setSenha("123");
		u.setIsAluno(false);
		
		usuarioService.save(u);
		
		Assert.assertTrue(u.getId() > 0);
	}
	
	@Test
	public void testLogin() throws NoSuchAlgorithmException, UnsupportedEncodingException {
		Usuario u = usuarioService.findUsuarioByLoginAndSenha("admin", "123");
		Assert.assertTrue(u != null);
	}
}
