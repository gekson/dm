/*

  DeepBlue Starter Kit - version 1.1
  Copyright (c) 2015 INMAGIK SRL - www.inmagik.com
  All rights reserved

  written by Mauro Bianchi
  bianchimro@gmail.com

  file: controllers.js
  description: this file contains all controllers of the DeepBlue app.

*/


//controllers are packed into a module
angular.module('deepBlue.controllers', [])

//top view controller
.controller('AppCtrl', function($scope, $rootScope, $state) {

  // #SIMPLIFIED-IMPLEMENTATION:
  // Simplified handling and logout function.
  // A real app would delegate a service for organizing session data
  // and auth stuff in a better way.
  $rootScope.user = {};

  $scope.logout = function(){
    $rootScope.user = {};
    $state.go('app.start')
  };

})

// This controller is bound to the "app.account" view
.controller('AccountCtrl', function($scope, $rootScope) {

  //readonly property is used to control editability of account form
  $scope.readonly = true;

  // #SIMPLIFIED-IMPLEMENTATION:
  // We act on a copy of the root user
  $scope.accountUser = angular.copy($rootScope.user);

  var userCopy = {};

  $scope.startEdit = function(){
    $scope.readonly = false;
    userCopy = angular.copy($scope.user);
  };

  $scope.cancelEdit = function(){
    $scope.readonly = true;
    $scope.user = userCopy;
  };

  // #SIMPLIFIED-IMPLEMENTATION:
  // this function should call a service to update and save
  // the data of current user.
  // In this case we'll just set form to readonly and copy data back to $rootScope.
  $scope.saveEdit = function(){
    $scope.readonly = true;
    $rootScope.user = $scope.accountUser;
  };

})


.controller('LoginCtrl', function ($scope, $state, $rootScope, $http, AWSEndpoint, popupFactory, CartService) {

  $scope.venda = {
    matricula : "",
    senha : ""
  };

  $scope.login = function(){
    $scope.cart = CartService.resetCart();
    $http({
  		method : 'POST',
          url: 'http://lowcost-env.y53tppagyf.us-west-2.elasticbeanstalk.com/usuario/findUsuarioByLoginAndSenha',
          data: {"matricula" : $scope.venda.matricula, "senha":$scope.venda.senha},
          headers: {'Access-Control-Allow-Origin': '*'}
          }).success(function(data) {
          	if(data.id) {
                  $rootScope.user = data;
                  $scope.user = data;
                  if(data.isAluno){
                    $http({
                      method : 'GET',
                      url: 'http://lowcost-env.y53tppagyf.us-west-2.elasticbeanstalk.com/ponto/getResgateAluno/'+data.aluno.id,
                      headers: {'Access-Control-Allow-Origin': '*'}
                    }).success(function(data2) {
                      $rootScope.codigoResgate = data2.codigo;
                    }).error(function(e) {
                      $rootScope.codigoResgate = "";
                      //popupFactory.showAlert($scope,"Erro", "Erro ao recuperar último Resgate " + e.message);
                    });
                    $state.go('app.shop');
                  }else{
                    $state.go('app.lanchonete');
                  }
              } else {
                  popupFactory.showAlert($scope,"Erro", "Usuário ou senha inválido.");
              }
          })
          .error(function(e) {
            popupFactory.showAlert($scope,"Erro", "Usuário ou senha inválido.");
          });    

  };
})

.controller('LanchoneteCtrl', function ($scope, $state, $rootScope, $http, popupFactory) {

  $scope.lanchonete = {
    "pontos" : 0,
    "matricula" : 0,
    "senha" : "",
    "image" : "sampledata/images/avatar.jpg"
  };

  $scope.usuario = {
    email : "maria@gmail.com",
    name : "Maria da Silva",
    address : "Rue de Galvignac",
    city : "RonnieLand",
    zip  : "00007",
    avatar : 'sampledata/images/avatar.jpg',
    menu : true
  };

  $scope.showAluno = false;

  $scope.buscaAluno = function(){
    $http({
  		method : 'POST',
          url: 'http://lowcost-env.y53tppagyf.us-west-2.elasticbeanstalk.com/aluno/findAlunoByMatricula',
          //data: {vm.produto, vm.upload}
          data: {"matricula":$scope.lanchonete.matricula}
          }).success(function(data) {
          	$scope.showAluno = true;
            $scope.usuario = data[0];
          })
          .error(function(e) {
            popupFactory.showAlert($scope,"Erro", "Aluno não encontrado.");
          	$scope.showAluno = false;
          });
  };

  $scope.validaTroca = function() {
    $http({
  		method : 'POST',
          url: 'http://lowcost-env.y53tppagyf.us-west-2.elasticbeanstalk.com/ponto/resgateLanchonete',
          //data: {vm.produto, vm.upload}
          data: {"matricula":$scope.lanchonete.matricula, "senha":$scope.lanchonete.senha, "pontos":$scope.lanchonete.pontos}
          }).success(function(data) {
            if(data==0){
              popupFactory.showAlert($scope,"Erro", "Saldo insuficiente.");
            }else{
                popupFactory.showAlert($scope,"Sucesso", "Troca realiza com sucesso.");
                $scope.lanchonete.pontos = 0;
                $scope.lanchonete.matricula = 0;
                $scope.lanchonete.senha = "";
                $scope.showAluno = false;
            }

          })
          .error(function(e) {
            popupFactory.showAlert($scope,"Erro", "Ocorreu um erro. Verifique se a Matricula e a senha estão corretas");
          });



  }
  // #SIMPLIFIED-IMPLEMENTATION:
  // This login function is just an example.
  // A real one should call a service that checks the auth against some
  // web service
  // $scope.venda = {
  //   matricula : "",
  //   senha : ""
  // };
  //
  // $scope.login = function(){
  //   if($scope.venda.matricula == "1") {
  //     $state.go('app.lanchonete');
  //   }else{
  //     //in this case we just set the user in $rootScope
  //     $rootScope.user = {
  //       email : "mary@ubiqtspaces.com",
  //       name : "Mary Ubiquitous",
  //       address : "Rue de Galvignac",
  //       city : "RonnieLand",
  //       zip  : "00007",
  //       avatar : 'sampledata/images/avatar.jpg'
  //     };
  //     //finally, we route our app to the 'app.shop' view
  //     $state.go('app.shop');
  //   }

  // };
})

.controller('ResgateCtrl', function ($scope, $state, $rootScope, $http, CartService, popupFactory) {
  $scope.cart = CartService.loadCart();
  $scope.cart.products = [];

  $scope.resgate = {
    "codigo" : "",
    "matricula" : 0,
    "senha" : "",
    "image" : "sampledata/images/avatar.jpg"
  };

  $scope.resgateRetorno = {
    "codigo" : "",
    "matricula" : 0,
    "senha" : "",
    "image" : "sampledata/images/avatar.jpg"
  };

  $scope.usuario = {
    email : "maria@gmail.com",
    name : "Maria da Silva",
    address : "Rue de Galvignac",
    city : "RonnieLand",
    zip  : "00007",
    avatar : 'sampledata/images/avatar.jpg',
    menu : true
  };

  $scope.showAluno = false;

  $scope.buscaAluno = function(){
    $http({
  		method : 'POST',
          url: 'http://lowcost-env.y53tppagyf.us-west-2.elasticbeanstalk.com/aluno/findAlunoByMatricula',
          //data: {vm.produto, vm.upload}
          data: {"matricula":$scope.resgate.matricula}
          }).success(function(data) {
          	$scope.showAluno = true;
            $scope.usuario = data[0];
          })
          .error(function(e) {
            popupFactory.showAlert($scope,"Erro", "Aluno não encontrado");
          	$scope.showAluno = false;
          });
  };

  $scope.findResgateByCodigo = function(){
    $http({
  		method : 'POST',
          url: 'http://lowcost-env.y53tppagyf.us-west-2.elasticbeanstalk.com/ponto/findResgateByCodigo',
          //data: {vm.produto, vm.upload}
          data: {"codigo":$scope.resgate.codigo}
          }).success(function(data) {
            if(!data.resgateRealizado) {
              $scope.showResgate = true;
              $scope.resgateRetorno = data;
              $scope.cart.products = [];

              for (i = 0; i <  data.produtos.length; i++) {
                $scope.cart.products.push(data.produtos[i]);
              }

              CartService.saveCart($scope.cart);
            }else{
              popupFactory.showAlert($scope,"Erro", "Código já resgatado, por favor utilize outro.");
            }

          })
          .error(function(e) {
            popupFactory.showAlert($scope,"Erro", "Código não encontrado");
          	$scope.showResgate = false;
          });
  };

  $scope.validaTroca = function(){
    if($rootScope.user.id == null || $rootScope.user.id == undefined) {
      popupFactory.showAlert($scope,"Erro", "Sessão perdida, por favor, logue novamente.");
      return;
    }
    var idsProduto = [];
    for (var i = 0; i < $scope.cart.products.length; i++) {
  	  idsProduto.push($scope.cart.products[i].id);
    }

    var id = [];
	  id.push($scope.usuario.id);

    var user = [];
    user.push($rootScope.user.id);

    var matricula = [];
    matricula.push($scope.resgate.matricula);

    var senha = [];
    senha.push($scope.resgate.senha);
    $http({
      		method : 'POST',
              url: 'http://lowcost-env.y53tppagyf.us-west-2.elasticbeanstalk.com/ponto/usePonto',
              data: {"id" :id , "idProduto":idsProduto, "usuario":user, "matricula": matricula, "senha": senha}
              }).success(function(data) {
            	  if(data==0){
                  popupFactory.showAlert($scope,"Erro", "Saldo insuficiente.");
            	  }else {
                  popupFactory.showAlert($scope,"Sucesso", "Troca realizada com sucesso.");
                  $scope.cart.products = [];
                  $scope.showResgate = false;
                  $scope.showAluno = false;
                  $http({
                    		method : 'POST',
                            url: 'http://lowcost-env.y53tppagyf.us-west-2.elasticbeanstalk.com/ponto/resgateRealizado',
                            data: {"id" :$scope.resgateRetorno.id , "usuario":$rootScope.user.id}
                          }).success(function(data2) {


                            })
                            .error(function(e) {
                              popupFactory.showAlert($scope,"Erro", e.message);
                            });
            	  }
              })
              .error(function(e) {
                popupFactory.showAlert($scope,"Erro", "Ocorreu um erro. Verifique se a Matricula e a senha estão corretas");
              });
    // alert("Troca realizada!");
  }
})

// Feeds controller.
.controller('FeedsCtrl', function($scope, BackendService) {

  // #SIMPLIFIED-IMPLEMENTATION:
  // In this example feeds are loaded from a json file.
  // (using "getFeeds" method in BackendService, see services.js)
  // In your application you can use the same approach or load
  // feeds from a web service.

  $scope.getSaldoPontos = function(){
    return $scope.user.aluno.saldoPontos;
  }

  $scope.getPontos = function(){
    return $scope.user.aluno.pontos;
  }

  $scope.doRefresh = function(){
      BackendService.getFeeds()
      .success(function(newItems) {
        $scope.feeds = newItems;
      })
      .finally(function() {
        // Stop the ion-refresher from spinning
        $scope.$broadcast('scroll.refreshComplete');
      });
  };

  // Triggering the first refresh
  $scope.doRefresh();

})

// Shop controller.
.controller('ShopCtrl', function($scope, $ionicActionSheet, BackendService, CartService) {

  // In this example feeds are loaded from a json file.
  // (using "getProducts" method in BackendService, see services.js)
  // In your application you can use the same approach or load
  // products from a web service.

  //using the CartService to load cart from localStorage
  $scope.cart = CartService.loadCart();

  $scope.doRefresh = function(){
      BackendService.getProducts()
      .success(function(newItems) {
        console.log(newItems);
        $scope.products = newItems;
      })
      .error(function(){
      console.log('data error');
    })
      .finally(function() {
        // Stop the ion-refresher from spinning (not needed in this view)
        $scope.$broadcast('scroll.refreshComplete');
      });
  };

  // private method to add a product to cart
  var addProductToCart = function(product){
    $scope.cart.products.push(product);
    CartService.saveCart($scope.cart);
    $scope.produto = product;
  };

  // method to add a product to cart via $ionicActionSheet
  $scope.addProduct = function(product){
    $ionicActionSheet.show({
       buttons: [
         { text: '<b>Adicionar ao carrinho</b>' }
       ],
       titleText: 'Trocar ' + product.nome,
       cancelText: 'Cancelar',
       cancel: function() {
          // add cancel code if needed ..
       },
       buttonClicked: function(index) {
         if(index == 0){
           addProductToCart(product);
           return true;
         }
         return true;
       }
     });
  };

  //trigger initial refresh of products
  $scope.doRefresh();

})

// controller for "app.cart" view
.controller('CartCtrl', function($scope, CartService, $ionicListDelegate, $ionicActionSheet, $http, popupFactory) {

  // using the CartService to load cart from localStorage
  $scope.cart = CartService.loadCart();
  $scope.showItem = false;

  // we assign getTotal method of CartService to $scope to have it available
  // in our template
  $scope.getTotal = CartService.getTotal;

  // removes product from cart (making in persistent)
  $scope.dropProduct = function($index){
    $scope.cart.products.splice($index, 1);
    CartService.saveCart($scope.cart);
    // as this method is triggered in an <ion-option-button>
    // we close the list after that (not strictly needed)
    $ionicListDelegate.closeOptionButtons();

  }

  $scope.produto = {
    "id" : 0,
    "title" : "TV",
    "description" : "Tv 43''",
    "image" : "sampledata/products/tvIlustrativa.jpg",
    "price" : 10
  };

  // private method to add a product to cart
  var addProductToCart = function(produto){
    $scope.cart.products.push(produto);
    CartService.saveCart($scope.cart);
    $scope.showItem = false;
  }

  // method to add a product to cart via $ionicActionSheet
  $scope.addProduct = function(produto){
    $http({
        		method : 'POST',
                url: 'http://lowcost-env.y53tppagyf.us-west-2.elasticbeanstalk.com/produto/getProduto',
                data: {'id':produto.id}
                }).success(function(data) {
                    addProductToCart(data);
                })
                .error(function(e) {
                  popupFactory.showAlert($scope,"Erro", "Produto não encontrado.");
                  return false;
                });

    return true;
    // $ionicActionSheet.show({
    //    buttons: [
    //      { text: '<b>Adicionar ao carrinho</b>' }
    //    ],
    //    titleText: 'Trocar ' + produto.title,
    //    cancelText: 'Cancelar',
    //    cancel: function() {
    //       // add cancel code if needed ..
    //    },
    //     buttonClicked: function(index) {
    //      if(index == 0){
    //        addProductToCart(produto);
    //        return true;
    //      }
    //      return true;
    //    }
    //  });
  };

  $scope.addItem = function(){
    $scope.showItem = true;
  }

})

.controller('CheckoutCtrl', function($scope, CartService, $state, $http, $rootScope, popupFactory) {

  //using the CartService to load cart from localStorage
  $scope.cart = CartService.loadCart();
  $scope.getTotal = CartService.getTotal;

  $scope.getTotal = CartService.getTotal;

  // #NOT-IMPLEMENTED: This method is just calling alert()
  // you should implement this method to connect an ecommerce
  // after that the cart is reset and user is redirected to shop
  $scope.checkout = function(){
    var idsProduto = [];
    for (var i = 0; i < $scope.cart.products.length; i++) {
  	  idsProduto.push($scope.cart.products[i].id);
    }

    var id = [];
	  id.push($scope.user.aluno.id);
    $http({
      		method : 'POST',
              url: 'http://lowcost-env.y53tppagyf.us-west-2.elasticbeanstalk.com/ponto/pedidoResgate',
              data: {"id" :id , "idProduto":idsProduto}
              }).success(function(data) {
            	  if(data==0){
                  popupFactory.showAlert($scope,"Erro", "Saldo insuficiente.");
            	  }else {
                  popupFactory.showAlert($scope,"Sucesso", "Pedido de resgate solicitado, compareça na escola com o número do pedido para verificar a disponibilidade dos produtos e realizar a troca. \n Seu código é: <b>"+ data.codigo+"</b>");
                  $scope.cart = CartService.resetCart();
                  $rootScope.codigoResgate = data.codigo;
            	  }
              })
              .error(function(e) {
                popupFactory.showAlert($scope,"Erro", "Os Pontos não foram usados. Ocorreu um erro. " + e.message);
              });
    // alert("Troca realizada!");
    $state.go('app.shop')
  }

})
